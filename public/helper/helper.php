<?php

function sendMail($expeditor,$recipient,$subject,$title,$message) {
	$expeditor = ($expeditor != '') ? $expeditor : '"Paris Est Ludique"<pafacil@gmail.com>';

	$header = "MIME-Version: 1.0\r\n";
	$header .= 'From:'.$expeditor."\n";
	$header .= 'Reply-to:'.$expeditor."\n";
	$header .= 'Content-Type:text/html; charset="utf-8"'."\n";
	$header .= 'Content-Transfer-Encoding: 8bit';		

	$message_html = 
		'<html>
			<body style="background-color:white;">
				<div style="width: 80%; margin:20px auto; box-shadow: 5px 0em 10px #000; border-radius: 10px; position:relative">
					<h1 style="
							background-color:black;
							margin: 0 auto;
							padding:10px;
							font-family: Grobold,Arial;
							font-size: 2em;
							text-align:center;
							color: white;
							text-stroke: 1px rgb(231,64,17);
							-webkit-text-stroke: 1px rgb(231,64,17);
							text-align: center;
							border-radius: 10px 10px 0 0;">
							<img src="https://forcebleue.depuisletemps.com/public/images/forceBleueLogo.png" alt="Logo ForceBleue" style="float: left" width="auto" height="70px">
							'.$title.'
					</h1>
					<div style="
						padding:20px; 
						margin:0;
						background-color: #bed3e3; 
						text-align: justify;
						border-radius: 0 0 10px 10px;">
						<p>
						'.$message.'
						</p>';

	if (isset($_POST['contact']) && $_POST['contact'] == "Envoyer") { 
		$message_html .= '
						<p style="text-align:right;line-height:100px;vertical-align:middle"><img src="https://forcebleue.depuisletemps.com/public/images/stamp.png" alt="Tampon du logo PEL" style="float: right; padding-left: 20px">
							<strong>PEL approved</strong>
						</p>';	
	} else {
		$message_html .= '
						<p style="text-indent: 30px">
						Cordialement,
						</p>
						<p style="text-align:right;line-height:100px;vertical-align:middle"><img src="https://forcebleue.depuisletemps.com/public/images/stamp.png" alt="Tampon du logo PEL" style="float: right; padding-left: 20px"><strong>L\'équipe Paris Est Ludique</strong>
						</p>';							
	}

	$message_html .= '
					</div>			
				</div>
			</body>
		</html>';

	if (mail($recipient, $subject, $message_html, $header)) {
		return 1;
	} else {
		return 0;
	}
}

function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    $pieces = [];
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $pieces []= $keyspace[rand(0, $max)];
    }
    return implode('', $pieces);
}

function isValidPassword($password) {
	$uppercase = preg_match('@[A-Z]@', $password);
	$lowercase = preg_match('@[a-z]@', $password);
	$number    = preg_match('@[0-9]@', $password);
	$specialChars = preg_match('@[#\$%&!?{}\@[\]\)\(]@', $password);

	$pwdErrors = [];
	
	if (strlen($password) < 20) {$pwdErrors[] = 'notExtraLong';}
	if (!$uppercase) {$pwdErrors[] = 'missingUppercase';}
	if (!$specialChars) {$pwdErrors[] = 'missingSpecial';}
	if (!$lowercase) {$pwdErrors[] = 'missingLowercase';}
	if (!$number) {$pwdErrors[] = 'missingNumber';}
	if (strlen($password) < 8) {$pwdErrors[] = 'tooShort';}

	if ((!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) && strlen($password) < 20) {
		$pwdErrors[] = 'badPwd';
	}

	return $pwdErrors;
}

function defaultPassword() {
		$defaultPassword = random_str(10);		
		$hashedPassword = password_hash($defaultPassword, PASSWORD_DEFAULT);
		$passwords = [$defaultPassword,$hashedPassword];
		return $passwords;
}

function temporaryMsg($msg) {
	$messages = [
		"contentUnsupportedType" => "Contenu - Extension non supportée<br>doc/docx/odt only",
		"contentFileTooLarge" => "Contenu - Fichier trop gros (limite 20 Mo)",
		"contentTransferError" => "Contenu - Erreur lors du transfert",
		"logoUnsupportedType" => "Logo - Extension non supportée<br>jpg/JPG/jpeg/JPEG/gif/GIF/png/PNG/psd/PSD",
		"logoFileTooLarge" => "Logo - Fichier trop gros (limite 20 Mo)",
		"logoTransferError" => "Logo - Erreur lors du transfert",		
		"updateOK" => "Informations enregistrées",		
		"exhibitorRemoved" => "Exposant et utilisateur associé supprimés",		
		"inexistingExhibitor" => "Exposant inexistant",		
		"userRemoved" => "Utilisateur et exposant associé supprimés",		
		"inexistingUser" => "Utilisateur inexistant",
		"alreadyExisting" => "Mail ou nom déjà utilisé<br> sur un autre compte",
		"missingField" => "Au moins l'un des champs requis était vide",			
		"createdEntry" => "Entrée ajoutée.",	
		"createdEntryMailSent" => "Entrée ajoutée.<br>Mail envoyé.",	
		"createdEntryMailFailed" => "Entrée ajoutée. <br>Erreur dans l'envoi du mail.",	
		"emptyMail" => "N'oubliez pas d'écrire votre message.",	
		"mailError" => "Erreur lors de l'envoi du message.<br>Merci de réessayer.<br>",
	 	"mailSent" => "Message envoyé"			
	];

	if ($msg == '') {
		if (isset($_GET['msg']) && $_GET['msg'] != '') {
			$msgType = trim(htmlspecialchars($_GET['msg']));
			return $messages[$msgType];
		} 
	} else {
		return $messages[$msg];
	}
}

function selection($type,$instance,$nb,$no_results_text,$placeholder,$windowsSize,$variableNameResult,$defaultReq) {
	$exhibManager = new ExhibitorManager();	
	$userManager = new UserManager();	

	if ($type == 'user' || $type == 'userAndExhib') {
		if ($placeholder == "") {
			if ($nb == 'single') {
				$placeholder = 'Sélectionnez un utilisateur';
			} else {
			$placeholder = 'Sélectionnez un/des utilisateur(s)';
			}
		}
		$resultArray = $userManager -> getList();

	} else if ($type == 'exhibitor' || $type == 'exhibitorAndUser') {
		if ($placeholder == "") {
			if ($nb == 'single') {
				$placeholder = 'Sélectionnez un exposant';
			} else {
			$placeholder = 'Sélectionnez un/des exposant(s)';
			}
		}
		
		$resultArray = $exhibManager -> getList(); 
	}  else if ($type == 'question') {
		$placeholder = 'À propos de...';
		
		$resultAssociativeArray = [
			'requete' => "J'ai une requête particulière",
			'boutique' => 'Je suis une boutique, modalités ?',
			'renseignements' => 'Renseignements pratiques',
			'tarifs' => 'À propos des tarifs',
			'autre' => 'Autre',			
		];
	} ?>

	<select data-placeholder="<?php echo $placeholder;?>..." name="<?php echo $variableNameResult; if ($nb=='multiple') {echo '[]';}?>" class="chosen-<?php echo $type.$instance;?>" <?php if ($nb!='single') {echo 'multiple';}?>>
		<option value="null"> </option>
		<?php
		if (isset ($resultArray)) {
			foreach ($resultArray as $object) { 
				// USER
				if ($type == 'user' || $type == 'userAndExhib') {	
					$userId = $object -> id();
					$firstName = $object -> firstName();
					$lastName = $object -> lastName();
					if (isset($selectedUser) AND (in_array($firstName.' '.$lastName, $selectedUser))) {
						$selected='selected';
					} else {
						$selected='';
					}				
					if ($type == 'userAndExhib') {
						if ($exhibitor = $exhibManager -> getExhibFromUser($object -> id())) {
							$exhibName = ' ('.$exhibitor -> name().')';
						} else {
							// In case this is an admin and not an exhibitor user
							$exhibName = ' (admin)';							
						}
					} else {
						$exhibName = '';
					}

					echo '<option '.$selected.' value="'.$userId.'">'.$firstName.' '.$lastName.$exhibName.' </option>';
				} 
				// EXHIBITOR
				if ($type == 'exhibitor' || $type == 'exhibitorAndUser') {	
					$exhibitorId = $object -> id();
					$name = $object -> name();
					if (isset($selectedExhibitor) AND (in_array($name, $selectedExhibitor))) {
						$selected='selected';
					} else {
						$selected='';
					}	

					if ($type == 'exhibitorAndUser') {
						$user = $userManager -> getUserFromExhib($object -> id());
						$userName = ' ('.$user -> firstName().' '.$user -> lastName().')';
					} else {
						$userName = '';
					}

					echo '<option '.$selected.' value="'.$exhibitorId.'">'.$name.$userName.' </option>';
				} 
			}
		} else if (isset ($resultAssociativeArray))	{	
			foreach ($resultAssociativeArray as $key => $value) { 
				if ($type == 'question') {
					echo '<option value="'.$key.'">'.$value.'</option>';
				}
			}		
		} ?>
	</select> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="public/chosen/chosen.jquery.js" type="text/javascript"></script>
	<script type="text/javascript">
		$('.chosen-<?php echo $type.$instance;?>').chosen({
			search_contains:true,
			allow_single_deselect:true,
			no_results_text:'<?php if ($no_results_text=='') {echo 'Pas de résultats';} else {echo $no_results_text;}?>',
			width:'<?php if ($windowsSize =='') {echo '450px';} else {echo $windowsSize;};?>'
			<?php if (isset ($max_choix)) { if ($max_choix != '') {echo ', max_selected_options: '.$max_choix;}};?>
		});
	</script>		
<?php }