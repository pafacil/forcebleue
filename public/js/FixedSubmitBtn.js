function FixedSubmitBtn() {
	this.adjustSubmitBtn = function() {
		var submitBtn = document.getElementById('submitBtn');
		var content = document.getElementById("content");

		if (content !== null) {
			// We check if "content+contentTop" is smallest than window less header/footer
			var heightHeaderFooter;
			if (window.matchMedia("(max-width: 500px)").matches) {
				heightHeaderFooter = 0;
			} else {
				heightHeaderFooter = 37;				
			}

			if ((content.getBoundingClientRect().height + content.offsetTop) > (window.innerHeight - heightHeaderFooter)) {
				submitBtn.classList.add("fixedSubmit");
			} else {
				submitBtn.classList.remove("fixedSubmit");			
			}
		}
	}
}