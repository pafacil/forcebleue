var Game = {
	init: function() {
		this.popTitleHeight = document.querySelector(".popTitleGame").scrollHeight;
		this.popGameLineHeight = document.getElementById("addGameLine").scrollHeight;
		this.initialMaxHeight = this.popTitleHeight + this.popGameLineHeight;

		this.activateDeleteButton();
		this.activateAddButton();
	},

	readjustMaxHeight: function() {
		var popContent = document.querySelector(".popContentGame");
		var stylePopContent = popContent.style.maxHeight +
		 this.popTitleHeight;

	    if (stylePopContent > this.initialMaxHeight){
	      popContent.style.maxHeight = this.initialMaxHeight;
	    } else {
	      popContent.style.maxHeight = popContent.scrollHeight + "px";
	    } 			
	},

	activateDeleteButton: function() {
		var that = this;
		this.gamesToDelete = document.getElementsByClassName("gameToBeDeleted")
		this.nbGamesToDelete = this.gamesToDelete.length;
		this.boothForm = document.getElementById("myBooth");

		for(i = 0 ; i < this.nbGamesToDelete; i++) {
			this.gamesToDelete[i].addEventListener('click', (function(index) {
				return function() {
				var id = this.id;

				var eltId = 'game' + id;				
				var lineToDelete = document.getElementById(eltId);

			    lineToDelete.parentNode.removeChild(lineToDelete);

				var newInput = document.createElement("input");
				newInput.type = 'hidden';
				newInput.value = id;
				newInput.name = 'gamesToDelete[]';		
				that.boothForm.appendChild(newInput); 		    
				}
			})(i));
		};		
	},

	activateDeleteButtonOnJustAddedGame: function() {
		this.gamesJustAddedToDelete = document.getElementsByClassName("btnToRemoveJustAddedGame")
		this.nbGamesJustAddedToDelete = this.gamesJustAddedToDelete.length;
		this.boothForm = document.getElementById("myBooth");

		for(i = 0 ; i < this.nbGamesJustAddedToDelete; i++) {
			this.gamesJustAddedToDelete[i].addEventListener('click', (function(index) {				
				return function() {
				// We remove the line the user added previously
				var idToRemove = 'remove' + this.id;
				var lineToDelete = document.getElementById(idToRemove);
			    lineToDelete.parentNode.removeChild(lineToDelete);	
				// We remove the line the hidden POST variable attached
				var idHiddenToRemove = 'hidden' + this.id;
				var lineHiddenToDelete = document.getElementById(idHiddenToRemove);
			    lineHiddenToDelete.parentNode.removeChild(lineHiddenToDelete);	
				}
			})(i));
		};		
	},

	activateAddButton: function() {
		var thisGameObject = this;
		this.gameToAdd = document.getElementById("addGameBtn");
		this.gameToAdd.addEventListener('click', function() {
			var newTitle = document.getElementById("newGameTitle").value;
			document.getElementById("newGameTitle").value = '';
			newTitle = newTitle.charAt(0).toUpperCase() + newTitle.substring(1).toLowerCase();	
			var newClassifications = document.getElementsByName('newGameClassification');
			var classification;
			var newClassification;
			var classificationNames = {
				classic: "Classique",
				new: "Nouveauté",
				proto: "Proto-Projet"
			}
			for(var i = 0; i < newClassifications.length; i++) {
				if (newClassifications[i].checked) {
					classification = newClassifications[i].value;
					newClassification = newClassifications[i].value.replace(/classic|new|proto/gi, function(matched){
					  return classificationNames[matched];
					});

					newClassifications[i].checked = false;
				}
			}

			if (newTitle != '' && typeof(newClassification) != "undefined") {
				var games = document.getElementsByClassName("gameTitle");
				var justAddedGames = document.getElementsByClassName("gameList");

				// We create an array with the existing game names
				gamesTitles = [];
				for (i = 0; i < games.length; i++) {
					gamesTitles.push(games[i].textContent.split(" (")[0].trim());
				};
				for (i = 0; i < justAddedGames.length; i++) {
					gamesTitles.push(justAddedGames[i].textContent.split(" (")[0].trim());
				};

				// If new entry does not already exist, we create a new line
				if (gamesTitles.indexOf(newTitle) == '-1') {
					var gameLine = document.createElement('div');
					var gameCell1 =  document.createElement('img');

					eltTitle = newTitle.replace(' ','');
					gameLine.id = 'remove' + eltTitle;
					gameLine.className = 'gameList'
					gameCell1.src = 'public/images/koS.png'
					gameCell1.alt= "Bouton Retirer Hôte"
					gameCell1.id = eltTitle;
					gameCell1.className = 'justAddedGame gameTitle hostIcon btnToRemoveJustAddedGame ' + eltTitle;
					gameLine.innerHTML += eltTitle + ' (' + newClassification + ')';
					gameLine.appendChild(gameCell1);

					document.getElementById("ourGames").appendChild(gameLine);

					boothForm = document.getElementById("myBooth");
					var newInput = document.createElement("input");
					newInput.type = 'hidden';
					newInput.value = classification;
					newInput.id = 'hidden'+eltTitle;
					newInput.name = 'gamesToAdd[' + eltTitle + ']';		
					boothForm.appendChild(newInput); 	

					thisGameObject.activateDeleteButtonOnJustAddedGame();
					thisGameObject.activateDeleteButton();
					thisGameObject.readjustMaxHeight();				
				}

			} else {
				let tempMessage = document.getElementById("tempMessage");

				// If there is no existing tempMessage div, we create one
				if (tempMessage == null) {	
					tempMessage = document.createElement('div');
					tempMessage.id = "tempMessage";
					document.body.insertBefore(tempMessage, document.getElementById('content'));
				}

				if (newTitle == '' && typeof newClassification == "undefined") {
					tempMessage.innerHTML = 'Rentrer le nom du jeu et son type avant de l\'ajouter';
				} else if (newTitle == '') {
					tempMessage.innerHTML = 'Il doit bien avoir un titre, ce jeu';
				} else if (typeof(newClassification == "undefined")) {
					tempMessage.innerHTML = 'Et c\'est une nouveauté ? un classique ?...';
				}

				if (newTitle == '' || typeof(newClassification == "undefined")) {
					tempMessage.style.display = 'block';
					tempMessage.style.opacity = '0';
					temporaryMessage()
				}
		
			}
		});
	},
}