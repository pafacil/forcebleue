var Counter = {
	init: function() {
		this.substractBtn();
		this.addBtn();
		this.validationBtn();
		},

	substractBtn: function() {
		this.lessBtn = document.getElementsByClassName("less");

		for(i = 0 ; i < this.lessBtn.length; i++) {
			this.lessBtn[i].addEventListener('click', (function(index) {
				return function() {
					var id = this.id;
					var packType = id.slice(0,-4);

					var numberElt = packType + 'Number';
				 	var number = document.getElementById(numberElt);		
				 	var currentNumber = parseInt(number.innerHTML);

					number.innerHTML = (currentNumber == 0) ? 0 : currentNumber - 1;
			 		currentNumber = (currentNumber == 0) ? 0 : currentNumber - 1;
				}
			})(i));
		}
	},

	addBtn: function() {
		this.moreBtn = document.getElementsByClassName("more");

		for(i = 0 ; i < this.moreBtn.length; i++) {
			this.moreBtn[i].addEventListener('click', (function(index) {
				return function() {
					var id = this.id;
					var packType = id.slice(0,-4);

					var numberElt = packType + 'Number';
				 	var number = document.getElementById(numberElt);		
				 	var currentNumber = parseInt(number.innerHTML);

				 	if (packType == "associationExterieur") {
						number.innerHTML = 1;
				 	} else {
						number.innerHTML = currentNumber + 1;
						currentNumber++;				 		
				 	}
				}
			})(i));
		}
	},	

	validationBtn: function() {
		var that = this;
		var myBoothBtn = document.getElementById("submitBtn");		
		var boothForm = document.getElementById("myBooth");
		var myOtherBoothBtn = document.getElementById("myOtherBoothBtn");

		myBoothBtn.addEventListener('click', function() {
			var specialValue = document.getElementById("specialNumber") ? document.getElementById("specialNumber").innerHTML : 'none';
			var newInput = document.createElement("input");
			newInput.type = 'hidden';
			newInput.value = specialValue;
			newInput.name = 'special';		
			boothForm.appendChild(newInput); 	

			var doublePackValue = document.getElementById("doublePackNumber") ? document.getElementById("doublePackNumber").innerHTML : 'none';
			var newInput = document.createElement("input");
			newInput.type = 'hidden';
			newInput.value = doublePackValue;
			newInput.name = 'doublePack';		
			boothForm.appendChild(newInput); 	

			var standardValue = document.getElementById("standardNumber") ? document.getElementById("standardNumber").innerHTML : 'none';
			var newInput = document.createElement("input");
			newInput.type = 'hidden';
			newInput.value = standardValue;
			newInput.name = 'standard';		
			boothForm.appendChild(newInput); 	

			var miniValue = document.getElementById("miniNumber") ? document.getElementById("miniNumber").innerHTML : 'none';
			var newInput = document.createElement("input");
			newInput.type = 'hidden';
			newInput.value = miniValue;
			newInput.name = 'mini';		
			boothForm.appendChild(newInput); 	

			var tablePackValue = document.getElementById("tablePackNumber") ? document.getElementById("tablePackNumber").innerHTML : 'none';
			var newInput = document.createElement("input");
			newInput.type = 'hidden';
			newInput.value = tablePackValue;
			newInput.name = 'tablePack';		
			boothForm.appendChild(newInput); 	

			var associationValue = document.getElementById("associationNumber") ? document.getElementById("associationNumber").innerHTML : 'none';
			var newInput = document.createElement("input");
			newInput.type = 'hidden';
			newInput.value = associationValue;
			newInput.name = 'association';		
			boothForm.appendChild(newInput); 	

			var associationExterieurValue = document.getElementById("associationExterieurNumber") ? document.getElementById("associationExterieurNumber").innerHTML : 'none';
			var newInput = document.createElement("input");
			newInput.type = 'hidden';
			newInput.value = associationExterieurValue;
			newInput.name = 'associationExterieur';		
			boothForm.appendChild(newInput);
		});
	},
}


