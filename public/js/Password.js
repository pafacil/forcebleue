var Password = {
	init: function() {
		var inputPwdField = document.getElementById("inputPassword");
		var inputPwdBisField = document.getElementById("inputPasswordBis");
		var lowerChk = document.getElementById("lowerChk");
		var upperChk = document.getElementById("upperChk");
		var numberChk = document.getElementById("numberChk");
		var specialChk = document.getElementById("specialChk");
		var lengthChk = document.getElementById("lengthChk");
		var ultraLengthChk = document.getElementById("ultraLengthChk");
		var identicalChk = document.getElementById("identicalChk");
		var loginBtn = document.getElementById("loginBtn");
		var pwdOk = 'abcde';
		var pwdDirectOk = 0;
		var pwdIdentical = 0;

		inputPwdField.onfocus = function() {
		    passwordRules.style.display = "block";
		};
		inputPwdBisField.onfocus = function() {
		    passwordRules.style.display = "block";
		};		

		inputPwdField.onblur = function() {
		    passwordRules.style.display = "none";
		};
		inputPwdBisField.onblur = function() {
		    passwordRules.style.display = "none";
		};

		inputPwdField.onkeyup = function() {
			var lowerCaseLetters = /[a-z]/g;
			if(inputPwdField.value.match(lowerCaseLetters)) {  
				lowerChk.classList.remove("invalid");
				lowerChk.classList.add("valid");
				pwdOk = pwdOk.replace("a", "A");
			} else {
				lowerChk.classList.remove("valid");
				lowerChk.classList.add("invalid");
				pwdOk = pwdOk.replace("A", "a");
			}						

			var upperCaseLetters = /[A-Z]/g;
			if(inputPwdField.value.match(upperCaseLetters)) {  
				upperChk.classList.remove("invalid");
				upperChk.classList.add("valid");
				pwdOk = pwdOk.replace("b", "B");
			} else {
				upperChk.classList.remove("valid");
				upperChk.classList.add("invalid");
				pwdOk = pwdOk.replace("B", "b");
			}	

			var numbers = /[0-9]/g;
			if(inputPwdField.value.match(numbers)) {  
				numberChk.classList.remove("invalid");
				numberChk.classList.add("valid");
				pwdOk = pwdOk.replace("c", "C");
			} else {
				numberChk.classList.remove("valid");
				numberChk.classList.add("invalid");
				pwdOk = pwdOk.replace("C", "c");
			}	

			var special = /[#$%&!@?{}\[\]()]/g;
			if(inputPwdField.value.match(special)) {  
				specialChk.classList.remove("invalid");
				specialChk.classList.add("valid");
				pwdOk = pwdOk.replace("d", "D");
			} else {
				specialChk.classList.remove("valid");
				specialChk.classList.add("invalid");
				pwdOk = pwdOk.replace("D", "d");
			}	

			if(inputPwdField.value.length > 7) {  
				lengthChk.classList.remove("invalid");
				lengthChk.classList.add("valid");
				pwdOk = pwdOk.replace("e", "E");
			} else {
				lengthChk.classList.remove("valid");
				lengthChk.classList.add("invalid");
				pwdOk = pwdOk.replace("E", "e");
			}

			if(inputPwdField.value.length > 19) {  
				ultraLengthChk.classList.remove("invalid");
				ultraLengthChk.classList.add("valid");
				pwdDirectOk = 1;
			} else {
				ultraLengthChk.classList.remove("valid");
				ultraLengthChk.classList.add("invalid");
				pwdDirectOk = 0;
			}			

			if(inputPwdField.value == inputPwdBisField.value && inputPwdField.value != '') {  
				identicalChk.classList.remove("invalid");
				identicalChk.classList.add("valid");
				pwdIdentical = 1;
			} else {
				identicalChk.classList.remove("valid");
				identicalChk.classList.add("invalid");
				pwdIdentical = 0;
			}

			if (pwdIdentical == '1' && (pwdOk == 'ABCDE' || pwdDirectOk == '1')) {
				loginBtn.classList.remove("inactive");
			    loginBtn.value = 'Valider';
			} else {
				loginBtn.classList.add("inactive");				
			    loginBtn.value = 'Mot de passe invalide';
			}
		};

		inputPwdBisField.onkeyup = function() {
			if(inputPwdField.value == inputPwdBisField.value && inputPwdField.value != '') {  
				identicalChk.classList.remove("invalid");
				identicalChk.classList.add("valid");
				pwdIdentical = 1;
			} else {
				identicalChk.classList.remove("valid");
				identicalChk.classList.add("invalid");
				pwdIdentical = 0;
			}	

			if (pwdIdentical == '1' && (pwdOk == 'ABCDE' || pwdDirectOk == '1')) {
				loginBtn.classList.remove("inactive");
			    loginBtn.value = 'Valider';
			} else {
				loginBtn.classList.add("inactive");				
			    loginBtn.value = 'Mot de passe invalide';
			}			
		}

		loginBtn = document.getElementById("loginBtn");
		loginBtn.addEventListener('click', function(e) {	
			// If passwords are not identical or if conditions are not met, we prevent validation		
			if (pwdIdentical == '0' || (pwdDirectOk == '0' && pwdOk != 'ABCDE')) {
				e.preventDefault();
			    passwordRules.style.display = "block";
			}
		});
	},


}