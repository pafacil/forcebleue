var YesNo = {
	init: function() {
		yesNoBtns = document.querySelectorAll('.flip-container');

		yesNoBtns.forEach(function(btn) {
			btn.addEventListener("click", function() {
				this.classList.toggle("flip");
				this.nextElementSibling.click();
			})
		});
		
	},
}