var Host = {
	init: function() {
		this.popTitleHeight = document.querySelector(".popTitleHost").scrollHeight;
		this.popHostLineHeight = document.getElementById("addHostLine").scrollHeight;
		this.initialMaxHeight = this.popTitleHeight + this.popHostLineHeight;

		this.activateDeleteButton();
		this.activateAddButton();
	},

	readjustMaxHeight: function() {
		var popContent = document.querySelector(".popContentHost");
		var stylePopContent = popContent.style.maxHeight +
		 this.popTitleHeight;

	    if (stylePopContent > this.initialMaxHeight){
	      popContent.style.maxHeight = this.initialMaxHeight;
	    } else {
	      popContent.style.maxHeight = popContent.scrollHeight + "px";
	    } 			
	},

	activateDeleteButton: function() {
		var that = this;
		this.hostsToDelete = document.getElementsByClassName("hostToBeDeleted");
		this.nbHostsToDelete = this.hostsToDelete.length;
		this.boothForm = document.getElementById("myBooth");

		for(i = 0 ; i < this.nbHostsToDelete; i++) {
			this.hostsToDelete[i].addEventListener('click', (function(index) {
				return function() {
				var id = this.id;

				var eltId = 'host' + id;				
				var lineToDelete = document.getElementById(eltId);
			    lineToDelete.parentNode.removeChild(lineToDelete);

				var newInput = document.createElement("input");
				newInput.type = 'hidden';
				newInput.value = id;
				newInput.name = 'hostsToDelete[]';		
				that.boothForm.appendChild(newInput); 			    
				}
			})(i));
		};		
	},

	activateDeleteButtonOnJustAddedHost: function() {
		this.hostsJustAddedToDelete = document.getElementsByClassName("btnToRemoveJustAddedHost")
		this.nbHostsJustAddedToDelete = this.hostsJustAddedToDelete.length;
		this.boothForm = document.getElementById("myBooth");

		for(i = 0 ; i < this.nbHostsJustAddedToDelete; i++) {
			this.hostsJustAddedToDelete[i].addEventListener('click', (function(index) {				
				return function() {
				// We remove the line the user added previously
				var idToRemove = 'removeHost' + this.id;
				var lineToDelete = document.getElementById(idToRemove);
			    lineToDelete.parentNode.removeChild(lineToDelete);	
				// We remove the line the hidden POST variable attached
				var idHiddenToRemove = 'hiddenHost' + this.id;
				var lineHiddenToDelete = document.getElementById(idHiddenToRemove);
			    lineHiddenToDelete.parentNode.removeChild(lineHiddenToDelete);	
				}
			})(i));
		};		
	},

	activateAddButton: function() {
		var thisHostObject = this;
		this.hostToAdd = document.getElementById("addHostBtn");

		this.hostToAdd.addEventListener('click', function() {
			var newName = document.getElementById("newHostName").value;
			document.getElementById("newHostName").value = '';
			newName = newName.charAt(0).toUpperCase() + newName.substring(1).toLowerCase();	

			if (newName != '') {
				var existingHost = document.getElementsByClassName("hostList");

				hostNames = [];
				for (i = 0; i < existingHost.length; i++) {
					hostNames.push(existingHost[i].textContent.trim());
				};

				if (hostNames.indexOf(newName) == '-1') {
					var hostLine = document.createElement('div');
					var hostCell1 =  document.createElement('img');

					eltName = newName.replace(' ','');
					hostLine.id = 'removeHost' + eltName;
					hostLine.className = 'hostList'
					hostCell1.src = 'public/images/koS.png'
					hostCell1.alt= "Bouton Retirer Hôte"
					hostCell1.id = eltName;
					hostCell1.className = 'justAddedHost hostName hostIcon btnToRemoveJustAddedHost ' + eltName;
					hostLine.innerHTML += eltName;
					hostLine.appendChild(hostCell1);

					document.getElementById("ourHosts").appendChild(hostLine);

					var popContentToExtend = document.getElementById('hosts');
					popContentToExtend.style.maxHeight = popContentToExtend.scrollHeight + "px";

					boothForm = document.getElementById("myBooth");
					var newInput = document.createElement("input");
					newInput.type = 'hidden';
					newInput.value = eltName;
					newInput.id = 'hiddenHost'+eltName;
					newInput.name = 'hostsToAdd[]';		
					boothForm.appendChild(newInput); 	

					thisHostObject.activateDeleteButtonOnJustAddedHost();
					thisHostObject.activateDeleteButton();
				}

			} else {
				let tempMessage = document.getElementById("tempMessage");
				if (typeof tempMessage == 'undefined') {	
					tempMessage = document.createElement('div');
					tempMessage.id = "tempMessage";
				}
				if (newName == '') {
					tempMessage.innerHTML = 'Rentrer un <span class="stroke orange">nom</span> avant de cliquer pour l\'ajouter';
				} 
				tempMessage.style.display = 'block';
				temporaryMessage();
			}
		});
	},
}