if (typeof Password != "undefined") {Password.init();}
if (typeof Game != "undefined") {Game.init();}
if (typeof Host != "undefined") {Host.init();}
if (typeof Counter != "undefined") {Counter.init();}
if (typeof Pops != "undefined") {Pops.init();}
if (typeof Select != "undefined") {Select.init();}
if (typeof YesNo != "undefined") {YesNo.init();}

// Temporary message management
function temporaryMessage() {
	var tempMessage = document.getElementById("tempMessage");
	$('#tempMessage').animate({opacity: 1}, 1000);
	setTimeout(function () {
	  $('#tempMessage').animate({opacity: 0}, 1000)
	  setTimeout(function () {
	     tempMessage.style.display = 'none';
	  }, 1500)
	},2000)
}
var tempMessage = document.getElementById("tempMessage");
if (typeof tempMessage != 'undefined' && tempMessage !== null && tempMessage !== '') {
	temporaryMessage();
}

// Actions
var addExhibitorBtn = document.getElementById("addExhibitorBtn");
var removeExhibitorBtn = document.getElementById("removeExhibitorBtn");
var addUserBtn = document.getElementById("addUserBtn");
var removeUserBtn = document.getElementById("removeUserBtn");
var addExhibitor = document.getElementById("addExhibitor");
var removeExhibitor = document.getElementById("removeExhibitor");
var addUser = document.getElementById("addUser");
var removeUser = document.getElementById("removeUser");
var addUserFirstFocus = document.getElementById("addUserFirstFocus");
var addExhibitorFirstFocus = document.getElementById("addExhibitorFirstFocus");

if (addExhibitorBtn !== null) {
	addExhibitorBtn.addEventListener("click", function() {
		if (addExhibitor.classList.contains("closed")) {
			setTimeout(function() {
				addExhibitorFirstFocus.focus();
			},300)
		} 
		addExhibitor.classList.remove("closed");
		removeExhibitor.classList.add("closed");
		addUser.classList.add("closed");
		removeUser.classList.add("closed");	
	});

	removeExhibitorBtn.addEventListener("click", function() {
		addExhibitor.classList.add("closed");
		removeExhibitor.classList.remove("closed");
		addUser.classList.add("closed");
		removeUser.classList.add("closed");	
	});

	addUserBtn.addEventListener("click", function() {
		if (addUser.classList.contains("closed")) {
			setTimeout(function() {
				addUserFirstFocus.focus();
			},300)	
		}
		addExhibitor.classList.add("closed");
		removeExhibitor.classList.add("closed");
		addUser.classList.remove("closed");
		removeUser.classList.add("closed");	
	});

	removeUserBtn.addEventListener("click", function() {
		addExhibitor.classList.add("closed");
		removeExhibitor.classList.add("closed");
		addUser.classList.add("closed");
		removeUser.classList.remove("closed");	
	});
}

// Arrow to top
$(document).ready(function(){  
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
	  if ($(this).scrollTop() > 200) {
	     $('.arrowToTop').addClass("On");
	  } else {
	     $('.arrowToTop').removeClass("On");
	  }
	});
   
	//Click event to scroll to top
	$('.arrowToTop').click(function(){
	  $('html, body').animate({scrollTop : 0},800);
	  return false;
	});  
});

// Section for upload button behavior
var upload = document.querySelector('.upload');
var uploadFilename = document.querySelector('.uploadFilename');

if (upload !== null) {
	upload.addEventListener('change', function(e) {
	   uploadFilename.innerHTML = event.target.files[0].name;
	});
}

if (typeof FixedSubmitBtn != 'undefined') {
	var fixSubmitBtn = new FixedSubmitBtn;
	if (fixSubmitBtn !== null) {
		setTimeout(function() {
		fixSubmitBtn.adjustSubmitBtn();
		}, 300);
	}
}

// To prevent to validate inputs with Return key (except on login screen)
var inputs = document.getElementsByTagName('input');
var login = document.getElementById('login');
if (inputs !== null && login === null) {
	for (var i=0; i < inputs.length; i++) {
		inputs[i].addEventListener("keydown", disableReturnOnForm)
	}
}

function disableReturnOnForm(event)
{
    // IE / Firefox
    if(!event && window.event) {
        event = window.event;
    }
    // IE
    if(event.keyCode == 13) {
        event.returnValue = false;
        event.cancelBubble = true;
    }
    // DOM
    if(event.which == 13) {
        event.preventDefault();
        event.stopPropagation();
    }
}