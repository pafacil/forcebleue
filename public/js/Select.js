var Select = {
	init: function() {
		this.thereIsASelection = false;
		this.imgSelect = document.getElementById("selectionIcon");
		this.select = document.getElementById("selectExhib").querySelector('div');
		
		if (window.matchMedia("(min-width: 461px)").matches) {
			this.selectDiv();
		}
	},

	selectDiv: function() {
		var thisSelectDiv = this;

		this.select.addEventListener('click', function() {
			if (thisSelectDiv.testSelection()) {
				thisSelectDiv.hideSelectedSpan();
			} else if (thisSelectDiv.thereIsASelection) {
				thisSelectDiv.displayAllLines();
				thisSelectDiv.thereIsASelection = false;
			} 
		});

		this.select.addEventListener('mouseover', function() {
			if (thisSelectDiv.testSelection()) {
				thisSelectDiv.hideSelectedSpan();
			} else if (thisSelectDiv.thereIsASelection) {
				thisSelectDiv.displayAllLines();
				thisSelectDiv.thereIsASelection = false;
			} 
		});		

		this.select.addEventListener('mouseout', function() {
			if (thisSelectDiv.testSelection()) {
				thisSelectDiv.hideSelectedSpan();
			} else if (thisSelectDiv.thereIsASelection) {
				thisSelectDiv.displayAllLines();
				thisSelectDiv.thereIsASelection = false;
			} 
		});	
	},

	testSelection: function() {
		anySelection = document.getElementById("selectExhib").querySelector('li.search-choice span');
		if (!!anySelection) {
			this.imgSelect.src = 'public/images/selectionOn.png'; 
			return true;
		} else {
			this.imgSelect.src = 'public/images/selectionOff.png';
			return false;
		}
	},

	hideSelectedSpan: function() {
		var that = this;
		allLines = document.querySelectorAll('.exhibLine');
		selection = document.getElementById("selectExhib").querySelectorAll('li.search-choice span');

		selectedLines = [];
		selection.forEach(function(span) {
			selectedLines.push(span.textContent.trim())
		});	

		let typeLine = false;
		allLines.forEach(function(line) {		
			exhibName = line.querySelector('td').textContent;
			if (selectedLines.length > 0) {
				that.thereIsASelection = true;
				let formElt = document.querySelector('form');
				if (!selectedLines.includes(exhibName)) {
					line.classList.add('hidden');
					line.classList.remove('selectedLine');
					if (!that.isHidden(line.id) && formElt != null) {
						var newInput = document.createElement("input");
						newInput.type = 'hidden';
						newInput.value = line.id;
						newInput.id = line.id +'hidden';
						newInput.name = 'lineNotToUpdate[]';		
						document.getElementById("exhibitorsStatusForm").appendChild(newInput);
					}
				} else {
					line.classList.remove('hidden');
					line.classList.add('selectedLine');
					(typeLine) ? line.style.background = '#90b5cf' : line.style.background = '#d8e5ee';
					typeLine = !typeLine;
					that.removeHiddenInput(line);
				}
			} 
		});	
		if (typeof FixedSubmitBtn != 'undefined') {
			this.fixSubmitBtn = new FixedSubmitBtn;
			setTimeout(function() {
				fixSubmitBtn.adjustSubmitBtn();	    
			},200);		
		}
	},

	isHidden: function(id) {
		let hiddenElts = document.querySelectorAll('input[type=hidden]');

		let hiddenIds = [];
		hiddenElts.forEach(function(elt) {
			hiddenIds.push(elt.id);
		});

		if (hiddenIds.includes(id+'hidden')) {return true;} else {return false;}
	},

	removeHiddenInput : function(line) {
		if (this.isHidden(line.id)) {
		 	var hiddenInputToDelete = document.getElementById(line.id+'hidden');
		 	hiddenInputToDelete.parentNode.removeChild(hiddenInputToDelete);
		}
	},

	displayAllLines : function() {
		let that = this;
		let hiddenInputElts = document.querySelectorAll('input[type=hidden]');
		let allLines = document.querySelectorAll('.exhibLine');
		let typeLine = false;

		// We remove all the hidden input element
		hiddenInputElts.forEach(function(elt) {
			var hiddenInputToDelete = document.getElementById(elt.id);
		 	hiddenInputToDelete.parentNode.removeChild(hiddenInputToDelete);	
		});

		// We show all the lines
		allLines.forEach(function(line) {
			line.classList.remove("hidden");
			line.classList.add("selectedLines");
			(typeLine) ? line.style.background = '#90b5cf' : line.style.background = '#d8e5ee';
			typeLine = !typeLine;
		});

		if (typeof FixedSubmitBtn != 'undefined') {
			this.fixSubmitBtn = new FixedSubmitBtn;
			setTimeout(function() {
				fixSubmitBtn.adjustSubmitBtn();	    
			},200);		
		}
	}
}

