var Pops = {
	init: function() {
		setTimeout(this.setMaxHeightOpen(), 500);		
		this.popBtn();
	},

	// Set Max-Height to scrollHeight for originally open pop
	setMaxHeightOpen: function() {
		var that = this;
		this.popsOpen = document.getElementsByClassName('open');

		if (this.popsOpen.length > 0) {				
			for(i = 0 ; i < this.popsOpen.length; i++) {
				this.popsOpen[i].style.maxHeight = this.popsOpen[i].scrollHeight + "px";
			};
		}
	},

	popBtn: function() {
		this.pops = document.getElementsByClassName("popTitle");
		this.nbPops = this.pops.length;
		if (typeof FixedSubmitBtn != 'undefined') {this.fixSubmitBtn = new FixedSubmitBtn;}

		for(i = 0 ; i < this.nbPops; i++) {
			this.pops[i].addEventListener('click', function() {
				var arrow = this.firstChild;
				arrow.classList.toggle('rotate180');

				var popContent = this.nextElementSibling;
				var stylePopContent = popContent.style.maxHeight;

			    if (stylePopContent){
			      popContent.style.maxHeight = null;
			    } else {
			      popContent.style.maxHeight = popContent.scrollHeight + "px";
			    } 	
				if (typeof FixedSubmitBtn != 'undefined') {
					setTimeout(function() {
						fixSubmitBtn.adjustSubmitBtn();	    
					},200);
				}	
			});
		}
	}
}