var contentTextareaBtn = document.getElementById("contentTextareaBtn");
var contentTextarea = document.getElementById("contentTextarea");
var down = document.getElementById("down");

var contentFileBtn = document.getElementById("contentFileBtn");
var contentFile = document.getElementById("contentFile");
var downBis = document.getElementById("downBis");

contentTextareaBtn.addEventListener("click", function(e){
   contentFile.classList.add("invisible");
   contentTextarea.classList.toggle("invisible");
   down.classList.toggle("rotate180");   
   if (downBis.classList.contains('rotate180')) {
      downBis.classList.toggle("rotate180");      
   }
});

contentFileBtn.addEventListener("click", function(e){
   contentTextarea.classList.add("invisible"); 
   contentFile.classList.toggle("invisible");
   downBis.classList.toggle("rotate180");   
   if (down.classList.contains('rotate180')) {
      down.classList.toggle("rotate180");      
   }
});