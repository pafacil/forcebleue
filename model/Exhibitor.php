<?php
class Exhibitor 
{
	private $_id;
  private $_userId;
	private $_name;
	private $_type;
	private $_company;
  private $_street;
  private $_postalCode;
  private $_city;
  private $_country;
  private $_contractor;
	private $_vatNumber;
  private $_firstName1;
  private $_lastName1;
  private $_mail1;
  private $_phone1;
  private $_firstName2;
  private $_lastName2;
  private $_mail2;
  private $_phone2;
	private $_webUrl;
	private $_facebook;
	private $_twitter;
	private $_logo;
  private $_area;
  private $_shop;
  private $_comment;
  private $_content;
  private $_contentDoc;
  private $_invitation;
  private $_pass;
  private $_confirmation;
  private $_alpha;
  private $_signed;
  private $_stand;  
	private $_creationDate;

  public function __construct(array $data)
  {
    $this->hydrate($data);
  }
  
// Hydration
	public function hydrate(array $data)
	{
		foreach ($data as $key => $value) {
			$method = 'set'.ucfirst($key);
			if(method_exists($this, $method))
			{
				$this -> $method($value);
			}
		}
	}


// Getters
  public function id()
  {
    return $this->_id;
  }

  public function userId()
  {
    return $this->_userId;
  }

  public function name()
  {
    return $this->_name;
  }

  public function type()
  {
    return $this->_type;
  }  

  public function company()
  {
    return $this->_company;
  } 

  public function street()
  {
    return $this->_street;
  } 

  public function postalCode()
  {
    return $this->_postalCode;
  } 

  public function city()
  {
    return $this->_city;
  } 

  public function country()
  {
    return $this->_country;
  } 

  public function contractor()
  {
    return $this->_contractor;
  } 

  public function vatNumber()
  {
    return $this->_vatNumber;
  } 

  public function firstName1()
  {
    return $this->_firstName1;
  } 

  public function lastName1()
  {
    return $this->_lastName1;
  } 

  public function mail1()
  {
    return $this->_mail1;
  } 

  public function phone1()
  {
    return $this->_phone1;
  } 

  public function firstName2()
  {
    return $this->_firstName2;
  } 

  public function lastName2()
  {
    return $this->_lastName2;
  } 

  public function mail2()
  {
    return $this->_mail2;
  } 

  public function phone2()
  {
    return $this->_phone2;
  } 
 
  public function webUrl()
  {
    return $this->_webUrl;
  } 

  public function facebook()
  {
    return $this->_facebook;
  } 

  public function twitter()
  {
    return $this->_twitter;
  }     

  public function logo()
  {
    return $this->_logo;
  }       

  public function area()
  {
    return $this->_area;
  }

  public function shop()
  {
    return $this->_shop;
  } 

  public function comment()
  {
    return $this->_comment;
  } 

  public function content()
  {
    return $this->_content;
  } 

  public function contentDoc()
  {
    return $this->_contentDoc;
  } 

  public function invitation()
  {
    return $this->_invitation;
  } 

  public function pass()
  {
    return $this->_pass;
  } 

  public function confirmation()
  {
    return $this->_confirmation;
  }

  public function alpha()
  {
    return $this->_alpha;
  }

  public function signed()
  {
    return $this->_signed;
  } 

  public function stand()
  {
    return $this->_stand;
  } 

  public function creationDate()
  {
    return $this->_creationDate;
  }                 

// Setters
  public function setId($id)
  {
    $id = (int) $id;
    
    if ($id > 0)
    {
      $this->_id = $id;
    }
  }

  public function setUserId($userId)
  {
    $userId = (int) $userId;
    
    if ($userId > 0)
    {
      $this->_userId = $userId;
    }
  }

  public function setName($name)
  {
    if (is_string($name))
    {
      $this->_name = ucwords(trim(htmlspecialchars($name)));
    }
  }

  public function setType($type)
  {
    if ($type == "association" || $type == "boutique" || $type == "editeur")
    {
      $this->_type = $type;
    }
  }  

  public function setContractor($contractor)
  {
    if (is_string($contractor))
    {
      $this->_contractor = ucwords(trim(htmlspecialchars($contractor)));
    }
  }   

  public function setCompany($company)
  {
    if (is_string($company))
    {
      $this->_company = ucwords(trim(htmlspecialchars($company)));
    }
  } 

  public function setStreet($street)
  {
    if (is_string($street))
    {
      $this->_street = ucwords(trim(htmlspecialchars($street)));
    }
  } 

  public function setPostalCode($postalCode)
  {
    $postalCode = (int) $postalCode;
    
    if ($postalCode > 0)
    {
      $this->_postalCode = $postalCode;
    }
  }  

  public function setCity($city)
  {
    if (is_string($city))
    {
      $this->_city = ucwords(trim(htmlspecialchars($city)));
    }
  } 

  public function setCountry($country)
  {
    if (is_string($country))
    {
      $this->_country = ucwords(trim(htmlspecialchars($country)));
    }
  }   

  public function setVatNumber($vat)
  {
    $vat = (int) $vat;
    
    if ($vat > 0)
    {
      $this->_vatNumber = $vat;
    }
  }  

  public function setFirstName1($firstName1)
  {
    if (is_string($firstName1))
    {
      $this->_firstName1 = ucwords(trim(htmlspecialchars($firstName1)));
    }
  } 

  public function setLastName1($lastName1)
  {
    if (is_string($lastName1))
    {
      $this->_lastName1 = ucwords(trim(htmlspecialchars($lastName1)));
    }
  } 

  public function setMail1($mail1)
  {
    if (filter_var($mail1, FILTER_VALIDATE_EMAIL))
    {
      $this->_mail1 = $mail1;
    }
  } 

  public function setPhone1($phone1)
  {
    $regEx = '/^\+?[0-9]+/m';
    $validPhoneNumber = preg_match($regEx, $phone1);

    if ($validPhoneNumber)
    {
      $this->_phone1 = $phone1;
    } 
  }  

  public function setFirstName2($firstName2)
  {
    if (is_string($firstName2))
    {
      $this->_firstName2 = ucwords(trim(htmlspecialchars($firstName2)));
    }
  } 

  public function setLastName2($lastName2)
  {
    if (is_string($lastName2))
    {
      $this->_lastName2 = ucwords(trim(htmlspecialchars($lastName2)));
    }
  } 

  public function setMail2($mail2)
  {
    if (filter_var($mail2, FILTER_VALIDATE_EMAIL))
    {
      $this->_mail2 = $mail2;
    }
  } 

  public function setPhone2($phone2)
  {   
    $regEx = '/^\+?[0-9]+/m';
    $validPhoneNumber = preg_match($regEx, $phone2);

    if ($validPhoneNumber) {
      $this->_phone2 = $phone2;
    }
  }  

  public function setWebUrl($webUrl)
  {
    if (is_string($webUrl))
    {
      $this->_webUrl = trim(htmlspecialchars($webUrl));
    }
  }     

  public function setFacebook($facebook)
  {
    if (is_string($facebook))
    {
      $this->_facebook = trim(htmlspecialchars($facebook));
    }
  }  

  public function setTwitter($twitter)
  {
    if (is_string($twitter))
    {
      $this->_twitter = trim(htmlspecialchars($twitter));
    }
  }   

  public function setLogo($logo)
  {
    if (is_string($logo))
    {
      $this->_logo = trim(str_replace(' ','',$logo));
    }
  }   

  public function setArea($area)
  {
    if ($area == "village" || $area == "alice" || $area == "figurine" || $area == "role" || $area == "association" || $area == "boutique")
    {
      $this->_area = $area;
    }
  } 

  public function setShop($shop)
  {
    if ($shop == '0' || $shop == '1')
    {
      $this->_shop = $shop;
    }
  }   

  public function setComment($comment)
  {
    if (is_string($comment))
    {
      $this->_comment = ucfirst(trim(htmlspecialchars($comment)));
    }
  } 

  public function setContent($content)
  {
    if (is_string($content))
    {
      $this->_content = ucfirst(trim(htmlspecialchars($content)));
    }
  } 

  public function setContentDoc($contentDoc)
  {   
    if (is_string($contentDoc))
    {
      $this->_contentDoc = $contentDoc;
    }
  }  

  public function setInvitation($invitation)
  {
    $invitation = (int) $invitation;
    
    if ($invitation > 0)
    {
      $this->_invitation = $invitation;
    }
  }

  public function setPass($pass)
  {
    $pass = (int) $pass;
    
    if ($pass > 0)
    {
      $this->_pass = $pass;
    }
  }


  public function setConfirmation($confirmation)
  {
    if ($confirmation == false || $confirmation == true)
    {
      $this->_confirmation = $confirmation;
    }
  } 

  public function setAlpha($alpha)
  {
    if ($alpha == false || $alpha == true)
    {
      $this->_alpha = $alpha;
    }
  }   

  public function setSigned($signed)
  {
    if ($signed == false || $signed == true)
    {
      $this->_signed = $signed;
    }
  }  

  public function setStand($stand)
  {
    $stand = (int) $stand;
    
    if ($stand > 0)
    {
      $this->_stand = $stand;
    }
  }  

  public function setCreationDate($creationDate)
  {
	// Conditions to be added
      $this->_creationDate = $creationDate;
  }    
}
