<?php

class HostManager extends Manager
{
  public function add(Host $host)
  {
    $db = $this -> connectDb();

    $q = $db -> prepare('INSERT INTO host(name, exhibitorId, creationDate) VALUES(:name, :exhibitorId, NOW())');

    $q->bindValue(':name', $host->name());
    $q->bindValue(':exhibitorId', $host->exhibitorId());
    $q->execute();
  }

  public function update(Host $host)
  {
    $db = $this -> connectDb();    
    $q = $db->prepare('UPDATE host SET name = :name, exhibitorId = :exhibitorId WHERE id = :id');

    $q->bindValue(':id', $host->id());
    $q->bindValue(':name', $host->name());
    $q->bindValue(':exhibitorId', $host->exhibitorId());
    $q->execute();
  }

  public function delete(Host $host)
  {
    $db = $this -> connectDb();

    $db->exec('DELETE FROM host WHERE id = '.$host->id());
  }

  public function exists($info) {
  $db = $this -> connectDb();
    if (is_numeric($info)) 
    {     
      return (bool) $db->query('SELECT COUNT(*) FROM host WHERE id = '.$info)->fetchColumn();
    }
      
    return (bool) false;
  }

  public function isDuplicate($name,$exhibitorId) {
  $db = $this -> connectDb();
      return (bool) $db->query('SELECT COUNT(*) FROM host WHERE name = "'.$name.'" AND exhibitorId = '.$exhibitorId)->fetchColumn();
  }  

  public function getHost($input)
  {
    if (is_numeric($input)) // If numeric, we search against the id
      {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM host WHERE id = "'.$input.'"');
      $data = $q -> fetch(PDO::FETCH_ASSOC);

      return new Host($data);
    }
    return (bool) false;    
  }

  public function getHostsFromExhibitor($input)
  {
    $hosts = [];
    // We confirm that input is numeric
    if (is_numeric($input)) {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM host WHERE exhibitorId = "'.$input.'" ORDER BY name');

      while ($data = $q->fetch(PDO::FETCH_ASSOC))
      {
        $hosts[] = new Host($data);
      }

      return $hosts;
    }

    return false;
  }

  public function getList()
  {
    $hosts = [];

    $db = $this -> connectDb();
    $q = $db -> query('SELECT * FROM host ORDER BY exhibitorId');

    while ($data = $q->fetch(PDO::FETCH_ASSOC))
    {
      $hosts[] = new Host($data);
    }

    return $hosts;
  }
}
