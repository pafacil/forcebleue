<?php
class Host 
{
	private $_id;
  private $_exhibitorId;
  private $_name;
	private $_creationDate;

  public function __construct(array $data)
  {
    $this->hydrate($data);
  }
  
// Hydration
	public function hydrate(array $data)
	{
		foreach ($data as $key => $value) {
			$method = 'set'.ucfirst($key);
			if(method_exists($this, $method))
			{
				$this -> $method($value);
			}
		}
	}

// Getters
  public function id()
  {
    return $this->_id;
  }

  public function exhibitorId()
  {
    return $this->_exhibitorId;
  }

  public function name()
  {
    return $this->_name;
  }

  public function creationDate()
  {
    return $this->_creationDate;
  }             

// Setters
  public function setId($id)
  {
    $id = (int) $id;
    
    if ($id > 0)
    {
      $this->_id = $id;
    }
  }

  public function setExhibitorId($exhibitorId)
  {
    $exhibitorId = (int) $exhibitorId;
    
    if ($exhibitorId > 0)
    {
      $this->_exhibitorId = $exhibitorId;
    }
  }

  public function setName($name)
  {
    if (is_string($name))
    {
      $this->_name = $name;
    }
  }

  public function setCreationDate($creationDate)
  {
	// Conditions to be added
      $this->_creationDate = $creationDate;
  }    
}
