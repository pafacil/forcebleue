<?php

class PackManager extends Manager
{
  public function add(Pack $pack)
  {
    $db = $this -> connectDb();

    $q = $db -> prepare('INSERT INTO pack(exhibitorId, special, doublePack, standard, mini, tablePack, association, associationExterieur, boutique, creationDate) VALUES(:exhibitorId, :special, :doublePack, :standard, :mini, :tablePack, :association, :associationExterieur, :boutique, NOW())');

    $q->bindValue(':exhibitorId', $pack->exhibitorId());
    $q->bindValue(':special', $pack->special(), PDO::PARAM_INT);
    $q->bindValue(':doublePack', $pack->doublePack(), PDO::PARAM_INT);
    $q->bindValue(':standard', $pack->standard(), PDO::PARAM_INT);
    $q->bindValue(':mini', $pack->mini(), PDO::PARAM_INT);
    $q->bindValue(':tablePack', $pack->tablePack(), PDO::PARAM_INT);
    $q->bindValue(':association', $pack->association(), PDO::PARAM_INT);
    $q->bindValue(':associationExterieur', $pack->associationExterieur());
    $q->bindValue(':boutique', $pack->boutique());
    $q->execute();
  }

  public function update(Pack $pack)
  {
    $db = $this -> connectDb();    
    $q = $db->prepare('UPDATE pack SET exhibitorId = :exhibitorId, special = :special, doublePack = :doublePack, standard = :standard, mini = :mini, tablePack = :tablePack, association = :association, associationExterieur = :associationExterieur, boutique = :boutique WHERE id = :id');

    $q->bindValue(':id', $pack->id());
    $q->bindValue(':exhibitorId', $pack->exhibitorId());
    $q->bindValue(':special', $pack->special());
    $q->bindValue(':doublePack', $pack->doublePack());
    $q->bindValue(':standard', $pack->standard());
    $q->bindValue(':mini', $pack->mini());
    $q->bindValue(':tablePack', $pack->tablePack());
    $q->bindValue(':association', $pack->association());
    $q->bindValue(':associationExterieur', $pack->associationExterieur());
    $q->bindValue(':boutique', $pack->boutique());
    $q->execute();
  }

  public function delete(Pack $pack)
  {
    $db = $this -> connectDb();

    $db->exec('DELETE FROM pack WHERE id = '.$pack->id());
  }

  public function exists($info) {
  $db = $this -> connectDb();
    if (is_numeric($info)) 
    {     
      return (bool) $db->query('SELECT COUNT(*) FROM pack WHERE id = '.$info)->fetchColumn();
    }
      
    return (bool) false;
  }

  public function getPack($input)
  {
    if (is_numeric($input)) // If numeric, we search against the id
      {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM pack WHERE id = "'.$input.'"');
      $data = $q -> fetch(PDO::FETCH_ASSOC);

      return new Pack($data);
    }
    return (bool) false;    
  }

  public function getPackFromExhibitor($input)
  {
    // We confirm that input is numeric
    if (is_numeric($input)) {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM pack WHERE exhibitorId = "'.$input.'"');
      $data = $q -> fetch(PDO::FETCH_ASSOC);

      return new Pack($data);
    }

    return false;
  }

  public function getList()
  {
    $packs = [];

    $db = $this -> connectDb();
    $q = $db -> query('SELECT * FROM pack ORDER BY exhibitorId');

    while ($data = $q->fetch(PDO::FETCH_ASSOC))
    {
      $packs[] = new Pack($data);
    }

    return $packs;
  }
}
