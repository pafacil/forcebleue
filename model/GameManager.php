<?php

class GameManager extends Manager
{
  public function add(Game $game)
  {
    $db = $this -> connectDb();

    $q = $db -> prepare('INSERT INTO game(title, exhibitorId, classification, creationDate) VALUES(:title, :exhibitorId, :classification, NOW())');

    $q->bindValue(':title', $game->title());
    $q->bindValue(':exhibitorId', $game->exhibitorId());
    $q->bindValue(':classification', $game->classification());
    $q->execute();
  }

  public function update(Game $game)
  {
    $db = $this -> connectDb();    
    $q = $db->prepare('UPDATE game SET title = :title, exhibitorId = :exhibitorId, classification = :classification WHERE id = :id');

    $q->bindValue(':id', $game->id());
    $q->bindValue(':title', $game->title());
    $q->bindValue(':exhibitorId', $game->exhibitorId());
    $q->bindValue(':classification', $game->classification());
    $q->execute();
  }

  public function delete(Game $game)
  {
    $db = $this -> connectDb();

    $db->exec('DELETE FROM game WHERE id = '.$game->id());
  }

  public function exists($info) {
  $db = $this -> connectDb();
    if (is_numeric($info)) 
    {     
      return (bool) $db->query('SELECT COUNT(*) FROM game WHERE id = '.$info)->fetchColumn();
    }   
    return (bool) false;
  }

  public function isDuplicate($title,$exhibitorId) {
  $db = $this -> connectDb();
      return (bool) $db->query('SELECT COUNT(*) FROM game WHERE title = "'.$title.'" AND exhibitorId = '.$exhibitorId)->fetchColumn();
  }  

  public function getGame($input)
  {
    if (is_numeric($input)) // If numeric, we search against the id
      {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM game WHERE id = "'.$input.'"');
      $data = $q -> fetch(PDO::FETCH_ASSOC);

      return new Game($data);
    }
    return (bool) false;    
  }

  public function getGamesFromExhibitor($input)
  {
    $games = [];
    // We confirm that input is numeric
    if (is_numeric($input)) {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM game WHERE exhibitorId = "'.$input.'" ORDER BY title');

      while ($data = $q->fetch(PDO::FETCH_ASSOC))
      {
        $games[] = new Game($data);
      }

      return $games;
    }

    return false;
  }

  public function getList()
  {
    $games = [];

    $db = $this -> connectDb();
    $q = $db -> query('SELECT * FROM game ORDER BY exhibitorId');

    while ($data = $q->fetch(PDO::FETCH_ASSOC))
    {
      $games[] = new Game($data);
    }

    return $games;
  }
}
