<?php

class ExhibitorManager extends Manager
{
  public function add(Exhibitor $exhibitor)
  {
    $db = $this -> connectDb();

    $q = $db -> prepare('INSERT INTO exhibitor(name, userId, type, company, street, postalCode, city, country, contractor, vatNumber, firstName1, lastName1, mail1, phone1, firstName2, lastName2, mail2, phone2, webUrl, facebook, twitter, logo, area, shop, comment, content, contentDoc, invitation, pass, confirmation, alpha, signed, stand, creationDate) VALUES(:name, :userId, :type, :company, :street, :postalCode, :city, :country, :contractor, :vatNumber, :firstName1, :lastName1, :mail1, :phone1, :firstName2, :lastName2, :mail2, :phone2, :webUrl, :facebook, :twitter, :logo, :area, :shop, :comment, :content, :contentDoc, :invitation, :pass, :confirmation, :alpha, :signed, :stand, NOW())');

    $q->bindValue(':name', $exhibitor->name());
    $q->bindValue(':userId', $exhibitor->userId());
    $q->bindValue(':type', $exhibitor->type());
    $q->bindValue(':company', $exhibitor->company());
    $q->bindValue(':street', $exhibitor->street());
    $q->bindValue(':postalCode', $exhibitor->postalCode(), PDO::PARAM_INT);
    $q->bindValue(':city', $exhibitor->city());
    $q->bindValue(':country', $exhibitor->country());
    $q->bindValue(':contractor', $exhibitor->contractor());
    $q->bindValue(':vatNumber', $exhibitor->vatNumber(), PDO::PARAM_INT);
    $q->bindValue(':firstName1', $exhibitor->firstName1());
    $q->bindValue(':lastName1', $exhibitor->lastName1());
    $q->bindValue(':mail1', $exhibitor->mail1());
    $q->bindValue(':phone1', $exhibitor->phone1(), PDO::PARAM_INT);
    $q->bindValue(':firstName2', $exhibitor->firstName2());
    $q->bindValue(':lastName2', $exhibitor->lastName2());
    $q->bindValue(':mail2', $exhibitor->mail2());
    $q->bindValue(':phone2', $exhibitor->phone2(), PDO::PARAM_INT);
    $q->bindValue(':webUrl', $exhibitor->webUrl());    
    $q->bindValue(':facebook', $exhibitor->facebook());
    $q->bindValue(':twitter', $exhibitor->twitter());    
    $q->bindValue(':logo', $exhibitor->logo());
    $q->bindValue(':area', $exhibitor->area());    
    $q->bindValue(':shop', $exhibitor->shop());
    $q->bindValue(':comment', $exhibitor->comment());    
    $q->bindValue(':content', $exhibitor->content());
    $q->bindValue(':contentDoc', $exhibitor->contentDoc());
    $q->bindValue(':invitation', $exhibitor->invitation());
    $q->bindValue(':pass', $exhibitor->pass());
    $q->bindValue(':confirmation', $exhibitor->confirmation());
    $q->bindValue(':alpha', $exhibitor->alpha());
    $q->bindValue(':signed', $exhibitor->signed());
    $q->bindValue(':stand', $exhibitor->stand());    
    $q->execute();
  }

    public function update(Exhibitor $exhibitor)
  {
    $db = $this -> connectDb();    
    $q = $db->prepare('UPDATE exhibitor SET name = :name, userId = :userId, type = :type, company = :company, street = :street, postalCode = :postalCode, city = :city, country = :country, contractor = :contractor, vatNumber = :vatNumber, firstName1 = :firstName1, lastName1 = :lastName1, mail1 = :mail1, phone1 = :phone1, firstName2 = :firstName2, lastName2 = :lastName2, mail2 = :mail2, phone2 = :phone2, webUrl = :webUrl, facebook = :facebook, twitter = :twitter, logo = :logo, area = :area, shop = :shop, comment = :comment, content = :content, contentDoc = :contentDoc, invitation = :invitation, pass = :pass, confirmation = :confirmation, alpha = :alpha, signed = :signed, stand = :stand WHERE id = :id');

    $q->bindValue(':id', $exhibitor->id());
    $q->bindValue(':name', $exhibitor->name());
    $q->bindValue(':userId', $exhibitor->userId());
    $q->bindValue(':type', $exhibitor->type());
    $q->bindValue(':company', $exhibitor->company());
    $q->bindValue(':street', $exhibitor->street());
    $q->bindValue(':postalCode', $exhibitor->postalCode(), PDO::PARAM_INT);
    $q->bindValue(':city', $exhibitor->city());
    $q->bindValue(':country', $exhibitor->country());
    $q->bindValue(':contractor', $exhibitor->contractor());
    $q->bindValue(':vatNumber', $exhibitor->vatNumber(), PDO::PARAM_INT);
    $q->bindValue(':firstName1', $exhibitor->firstName1());
    $q->bindValue(':lastName1', $exhibitor->lastName1());
    $q->bindValue(':mail1', $exhibitor->mail1());
    $q->bindValue(':phone1', $exhibitor->phone1());
    $q->bindValue(':firstName2', $exhibitor->firstName2());
    $q->bindValue(':lastName2', $exhibitor->lastName2());
    $q->bindValue(':mail2', $exhibitor->mail2());
    $q->bindValue(':phone2', $exhibitor->phone2());
    $q->bindValue(':webUrl', $exhibitor->webUrl());    
    $q->bindValue(':facebook', $exhibitor->facebook());
    $q->bindValue(':twitter', $exhibitor->twitter());    
    $q->bindValue(':logo', $exhibitor->logo());
    $q->bindValue(':area', $exhibitor->area());    
    $q->bindValue(':shop', $exhibitor->shop());
    $q->bindValue(':comment', $exhibitor->comment());    
    $q->bindValue(':content', $exhibitor->content());
    $q->bindValue(':contentDoc', $exhibitor->contentDoc());
    $q->bindValue(':invitation', $exhibitor->invitation());
    $q->bindValue(':pass', $exhibitor->pass());    
    $q->bindValue(':confirmation', $exhibitor->confirmation());
    $q->bindValue(':alpha', $exhibitor->alpha());
    $q->bindValue(':signed', $exhibitor->signed());
    $q->bindValue(':stand', $exhibitor->stand());
    $q->execute();
  }

  public function delete($exhibitorId)
  {
    $exhibManager = new ExhibitorManager();
    $exhibitor = $exhibManager -> getExhibitor($exhibitorId);

    $db = $this -> connectDb();
    $db->exec('DELETE FROM game WHERE exhibitorId = '.$exhibitorId);
    $db->exec('DELETE FROM host WHERE exhibitorId = '.$exhibitorId);
    $db->exec('DELETE FROM pack WHERE exhibitorId = '.$exhibitorId);
    $db->exec('DELETE FROM user WHERE id = '.$exhibitor -> userId());  
    if ($exhibitor -> logo() != '') {
          $fileLocation = 'public/userData/logo/'.$exhibitor -> logo();
          unlink($fileLocation);
    } 
    if ($exhibitor -> contentDoc() != '') {
          $fileLocation = 'public/userData/contentDoc/'.$exhibitor -> contentDoc();
          unlink($fileLocation);
    }     
    $db->exec('DELETE FROM exhibitor WHERE id = '.$exhibitorId);
  }

  public function exists($info) {
  $db = $this -> connectDb();
    if (is_numeric($info)) 
    {     
      return (bool) $db->query('SELECT COUNT(*) FROM exhibitor WHERE id = '.$info)->fetchColumn();
    }
       
    $q = $db->prepare('SELECT COUNT(*) FROM exhibitor WHERE name = :name');
    $q->execute([':name' => $info]);
    
    return (bool) $q->fetchColumn();
  }

  public function getExhibitor($input)
  {
    if (is_numeric($input)) // If numeric, we search against the id
      {$field = 'id';}
    else // Else against the name
      {$field = 'name';}

    $db = $this -> connectDb();
    $q = $db -> query('SELECT * FROM exhibitor WHERE '.$field.' = "'.$input.'"');
    $data = $q -> fetch(PDO::FETCH_ASSOC);

    return new Exhibitor($data);
  }

  public function getExhibFromUser($input)
  {
    // We confirm that input is numeric
    if (is_numeric($input)) {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM exhibitor WHERE userId = "'.$input.'"');
      $data = $q -> fetch(PDO::FETCH_ASSOC);
      if ($data) {return new Exhibitor($data);}
    }
    return false;
  }

  public function getList()
  {
    $exhibitors = [];

    $db = $this -> connectDb();
    $q = $db -> query('SELECT * FROM exhibitor ORDER BY name');

    while ($data = $q->fetch(PDO::FETCH_ASSOC))
    {
      $exhibitors[] = new Exhibitor($data);
    }

    return $exhibitors;
  }
}
