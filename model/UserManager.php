<?php 

class UserManager extends Manager
{
	  public function exists($info) {
		$db = $this -> connectDb();
	    if (is_numeric($info)) 
	    {    	
	      return (bool) $db->query('SELECT COUNT(*) FROM user WHERE id = '.$info)->fetchColumn();
	    }
	       
	    $q = $db->prepare('SELECT COUNT(*) FROM user WHERE mail = :mail');
	    $q->execute([':mail' => $info]);
	    
	    return (bool) $q->fetchColumn();
	  }

	public function getUser($input) {
	    if (is_numeric ($input)) // If integer, we search against the id
	      {
	      	$field = 'id';}
	    else // Else against the name
	      {
	      	$field = 'mail';}
		$db = $this -> connectDb();
		$req_user = $db -> prepare('SELECT * FROM user WHERE '.$field.' = ?');
		$req_user -> execute(array($input));
		$user = $req_user -> fetch();

	    return new User($user);
	}	

  public function getUserFromExhib($input)
  {
  	$userManager = new UserManager();
    // We confirm that input is numeric
    if (is_numeric($input)) {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT userId FROM exhibitor WHERE id = "'.$input.'"');
      $data = $q -> fetch(PDO::FETCH_ASSOC);

      if ($data) {
      	$dataUser = $userManager -> getUser($data['userId']);
      	return $dataUser;
      }
    }
    return false;
  }	

	public function getList() {
	    $users = [];
	    $db = $this -> connectDb();
	    $q = $db -> query('SELECT * FROM user ORDER BY firstName, lastName');

	    while ($data = $q->fetch(PDO::FETCH_ASSOC))
	    {
	      $users[] = new User($data);
	    }

	    return $users;
	}

	public function checkPassword($mail, $password) {
		$db = $this -> connectDb();
		$req_password = $db -> prepare('SELECT password FROM user WHERE mail = ?');	
	    $req_password -> execute(array($mail));	
		$hashed_password = $req_password -> fetch();

		$isValidPassword = (password_verify($password, $hashed_password['0'])) ? true : false;

		return $isValidPassword;			
	}	

	public function add(User $user)	{
		$db = $this -> connectDb();

		$q = $db -> prepare('INSERT INTO user(mail, firstName, lastName, password, defaultPassword, privilege, creationDate) VALUES(:mail, :firstName, :lastName, :password, :defaultPassword, :privilege, NOW())');

		$q->bindValue(':mail', $user->mail());
		$q->bindValue(':firstName', $user->firstName());
		$q->bindValue(':lastName', $user->lastName());
		$q->bindValue(':password', $user->password());
		$q->bindValue(':defaultPassword', $user->defaultPassword());
		$q->bindValue(':privilege', $user->privilege());
		$q->execute();
	}

	public function update(User $user) {
		$db = $this -> connectDb();    
		$q = $db->prepare('UPDATE user SET mail = :mail, firstName = :firstName, lastName = :lastName, password = :password, defaultPassword = :defaultPassword, privilege = :privilege WHERE id = :id');

	    $q->bindValue(':id', $user->id());
		$q->bindValue(':firstName', $user->firstName());
		$q->bindValue(':lastName', $user->lastName());
		$q->bindValue(':mail', $user->mail());
		$q->bindValue(':password', $user->password());
		$q->bindValue(':defaultPassword', $user->defaultPassword());
		$q->bindValue(':privilege', $user->privilege());  

		$q->execute();
	}	

  public function delete($userId)
  {
    $exhibManager = new ExhibitorManager();
    $userManager = new UserManager();
    $user = $userManager -> getUser($userId);
    $exhibitor = $exhibManager -> getExhibFromUser($userId);
    $db = $this -> connectDb();

    if ($exhibitor) {
		$exhibitorId = $exhibitor -> id();
	    $db->exec('DELETE FROM game WHERE exhibitorId = '.$exhibitorId);
	    $db->exec('DELETE FROM host WHERE exhibitorId = '.$exhibitorId);
	    $db->exec('DELETE FROM pack WHERE exhibitorId = '.$exhibitorId);
	    $db->exec('DELETE FROM exhibitor WHERE id = '.$exhibitorId);    	
    }

    $db->exec('DELETE FROM user WHERE id = '.$userId);    
  }	
}