<?php

class User
{
	private $_id;
  private $_mail;
  private $_firstName;
  private $_lastName;
	private $_password;
  private $_defaultPassword;
	private $_privilege;
	private $_creationDate;

  public function __construct(array $data)
  {
    $this->hydrate($data);
  }

// Hydration
	public function hydrate(array $data)
	{
		foreach ($data as $key => $value) {
			$method = 'set'.ucfirst($key);
			if(method_exists($this, $method))
			{
				$this -> $method($value);
			}
		}
	}  	

// Getters
  public function id()
  {
    return $this->_id;
  }

  public function firstName()
  {
    return $this->_firstName;
  }

  public function lastName()
  {
    return $this->_lastName;
  }

  public function mail()
  {
    return $this->_mail;
  }

  public function password()
  {
    return $this->_password;
  } 

  public function defaultPassword()
  {
    return $this->_defaultPassword;
  } 

  public function privilege()
  {
    return $this->_privilege;
  } 

  public function creationDate()
  {
    return $this->_creationDate;
  } 

// Setters
  public function setId($id)
  {
    $id = (int) $id;
    
    if ($id > 0)
    {
      $this->_id = $id;
    }
  }

  public function setFirstName($firstName)
  {
    if (is_string($firstName))
    {
      $this->_firstName = $firstName;
    }
  }

  public function setLastName($lastName)
  {
    if (is_string($lastName))
    {
      $this->_lastName = $lastName;
    }
  }

  public function setMail($mail)
  {
    if (is_string($mail))
    {
    	$this->_mail = $mail;
    }
  }

  public function setPassword($password)
  {
   if (is_string($password))
    {  	
    	$this->_password = $password;
    }
  } 

  public function setDefaultPassword($defaultPassword)
  {
   if ($defaultPassword == '0' || $defaultPassword == '1')
    {   
      $this->_defaultPassword = $defaultPassword;
    }
  } 

    public function setPrivilege($privilege)
  {
    if ($privilege == "user" || $privilege == "newUser" || $privilege == "admin")
    {
      $this->_privilege = $privilege;
    }
  } 

  public function setCreationDate($creationDate)
  {
	// Conditions to be added
      $this->_creationDate = $creationDate;
  }   
}