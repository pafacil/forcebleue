<?php
class Game 
{
	private $_id;
  private $_exhibitorId;
  private $_title;
	private $_classification;
	private $_creationDate;

  public function __construct(array $data)
  {
    $this->hydrate($data);
  }
  
// Hydration
	public function hydrate(array $data)
	{
		foreach ($data as $key => $value) {
			$method = 'set'.ucfirst($key);
			if(method_exists($this, $method))
			{
				$this -> $method($value);
			}
		}
	}

// Getters
  public function id()
  {
    return $this->_id;
  }

  public function exhibitorId()
  {
    return $this->_exhibitorId;
  }

  public function title()
  {
    return $this->_title;
  }

  public function classification()
  {
    return $this->_classification;
  }

  public function creationDate()
  {
    return $this->_creationDate;
  }             

// Setters
  public function setId($id)
  {
    $id = (int) $id;
    
    if ($id > 0)
    {
      $this->_id = $id;
    }
  }

  public function setExhibitorId($exhibitorId)
  {
    $exhibitorId = (int) $exhibitorId;
    
    if ($exhibitorId > 0)
    {
      $this->_exhibitorId = $exhibitorId;
    }
  }

  public function setTitle($title)
  {
    if (is_string($title))
    {
      $this->_title = $title;
    }
  }

  public function setClassification($classification)
  {
    if ($classification == "classic" || $classification == "new" || $classification == "proto")
    {
      $this->_classification = $classification;
    }
  } 

  public function setCreationDate($creationDate)
  {
	// Conditions to be added
      $this->_creationDate = $creationDate;
  }    
}
