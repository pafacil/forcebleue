<?php

class Pack 
{
	private $_id;
  private $_exhibitorId;
	private $_special;
	private $_doublePack;
  private $_standard;
  private $_mini;
  private $_tablePack;
  private $_association;
  private $_associationExterieur;
	private $_boutique;
	private $_creationDate;

  public function __construct(array $data)
  {
    $this->hydrate($data);
  }
  
// Hydration
	public function hydrate(array $data)
	{
		foreach ($data as $key => $value) {
			$method = 'set'.ucfirst($key);
			if(method_exists($this, $method))
			{
				$this -> $method($value);
			}
		}
	}


// Getters
  public function id()
  {
    return $this->_id;
  }

  public function exhibitorId()
  {
    return $this->_exhibitorId;
  }

  public function special()
  {
    return $this->_special;
  }

  public function doublePack()
  {
    return $this->_doublePack;
  } 

  public function standard()
  {
    return $this->_standard;
  } 

  public function mini()
  {
    return $this->_mini;
  } 

  public function tablePack()
  {
    return $this->_tablePack;
  } 

  public function association()
  {
    return $this->_association;
  } 

  public function associationExterieur()
  {
    return $this->_associationExterieur;
  } 

  public function boutique()
  {
    return $this->_boutique;
  } 

  public function creationDate()
  {
    return $this->_creationDate;
  }             

// Setters
  public function setId($id)
  {
    $id = (int) $id;
    
    if ($id > 0)
    {
      $this->_id = $id;
    }
  }

  public function setExhibitorId($exhibitorId)
  {
    $exhibitorId = (int) $exhibitorId;
    
    if ($exhibitorId > 0)
    {
      $this->_exhibitorId = $exhibitorId;
    }
  }

  public function setSpecial($special)
  {
    $special = (int) $special;
    
    if ($special >= 0)
    {
      $this->_special = $special;
    }
  } 

  public function setDoublePack($doublePack)
  {
    $doublePack = (int) $doublePack;
    
    if ($doublePack >= 0)
    {
      $this->_doublePack = $doublePack;
    }
  }   

  public function setStandard($standard)
  {
    $standard = (int) $standard;
    
    if ($standard >= 0)
    {
      $this->_standard = $standard;
    }
  } 

  public function setMini($mini)
  {
    $mini = (int) $mini;
    
    if ($mini >= 0)
    {
      $this->_mini = $mini;
    }
  } 

  public function setTablePack($tablePack)
  {   
    $tablePack = (int) $tablePack;
    
    if ($tablePack >= 0)
    {
      $this->_tablePack = $tablePack;
    }
  }  

  public function setAssociation($association)
  {
    $association = (int) $association;
    
    if ($association >= 0)
    {
      $this->_association = $association;
    }
  }

  public function setAssociationExterieur($associationExterieur)
  {   
    $associationExterieur = (int) $associationExterieur;
    
    if ($associationExterieur >= 0)
    {
      $this->_associationExterieur = $associationExterieur;
    }
  }


  public function setBoutique($boutique)
  {   
    if ($boutique == "true" || $boutique == "false")
    {
      $this->_boutique = $boutique;
    }
  }

  public function setCreationDate($creationDate)
  {
	// Conditions to be added
      $this->_creationDate = $creationDate;
  }    
}
