<?php

class Booth 
{
	private $_id;
  private $_exhibitorId;
	private $_area;
	private $_shop;
  private $_comment;
  private $_content;
  private $_contentDoc;
  private $_invitation;
	private $_pass;
	private $_creationDate;

  public function __construct(array $data)
  {
    $this->hydrate($data);
  }
  
// Hydration
	public function hydrate(array $data)
	{
		foreach ($data as $key => $value) {
			$method = 'set'.ucfirst($key);
			if(method_exists($this, $method))
			{
				$this -> $method($value);
			}
		}
	}


// Getters
  public function id()
  {
    return $this->_id;
  }

  public function exhibitorId()
  {
    return $this->_exhibitorId;
  }

  public function area()
  {
    return $this->_area;
  }

  public function shop()
  {
    return $this->_shop;
  } 

  public function comment()
  {
    return $this->_comment;
  } 

  public function content()
  {
    return $this->_content;
  } 

  public function contentDoc()
  {
    return $this->_contentDoc;
  } 

  public function invitation()
  {
    return $this->_invitation;
  } 

  public function pass()
  {
    return $this->_pass;
  } 

  public function creationDate()
  {
    return $this->_creationDate;
  }             

// Setters
  public function setId($id)
  {
    $id = (int) $id;
    
    if ($id > 0)
    {
      $this->_id = $id;
    }
  }

  public function setExhibitorId($exhibitorId)
  {
    $exhibitorId = (int) $exhibitorId;
    
    if ($exhibitorId > 0)
    {
      $this->_exhibitorId = $exhibitorId;
    }
  }

  public function setArea($area)
  {
    if ($area == "village" || $area == "alice" || $area == "figurine" || $area == "role" || $area == "association" || $area == "boutique")
    {
      $this->_area = $area;
    }
  } 

  public function setShop($shop)
  {
    if (is_string($shop))
    {
      $this->_shop = $shop;
    }
  }   

  public function setComment($comment)
  {
    if (is_string($comment))
    {
      $this->_comment = $comment;
    }
  } 

  public function setContent($content)
  {
    if (is_string($content))
    {
      $this->_content = $content;
    }
  } 

  public function setContentDoc($contentDoc)
  {   
    if (is_string($contentDoc))
    {
      $this->_contentDoc = $contentDoc;
    }
  }  

  public function setInvitation($invitation)
  {
    $invitation = (int) $invitation;
    
    if ($invitation > 0)
    {
      $this->_invitation = $invitation;
    }
  }

  public function setPass($pass)
  {
    $pass = (int) $pass;
    
    if ($pass > 0)
    {
      $this->_pass = $pass;
    }
  }

  public function setCreationDate($creationDate)
  {
	// Conditions to be added
      $this->_creationDate = $creationDate;
  }    
}
