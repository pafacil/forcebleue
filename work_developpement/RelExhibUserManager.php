<?php 
class RelExhibUserManager extends Manager 
{
	public function add(RelExhibUser $rel)	{
		$db = $this -> connectDb();

		$q = $db -> prepare('INSERT INTO rel_exhibitor_user(exhibitorId, userId, creationDate) VALUES(:exhibitorId, :userId, NOW())');

		$q->bindValue(':exhibitorId', $rel->exhibitorId());
		$q->bindValue(':userId', $rel->userId());
		$q->execute();
	}

	public function update(RelExhibUser $rel) {
		$db = $this -> connectDb();    
		$q = $db->prepare('UPDATE rel_exhibitor_user SET exhibitorId = :exhibitorId, userId = :userId WHERE id = :id');

	    $q->bindValue(':id', $rel->id());
		$q->bindValue(':exhibitorId', $rel->exhibitorId());
		$q->bindValue(':userId', $rel->userId());
		$q->bindValue(':privilege', $user->privilege());  

		$q->execute();
	}	

	public function getExhibFromUser(User $user) {
	
		$db = $this -> connectDb();    
		$req_exhib = $db -> prepare('SELECT exhibitorId FROM rel_exhibitor_user WHERE userId = ?');
		$req_exhib -> execute(array($user -> id()));
		$exhib_id = $req_exhib -> fetch();
	    $exhibManager = new ExhibitorManager();
	    // We return the corresponding Exhibitor object
	    return $exhibManager -> getExhibitor($exhib_id['0']);
	}
}