<?php
class RelExhibAddress
{
	private $_id;
	private $_exhibitorId;
	private $_addressId;
	private $_creationDate;

  public function __construct(array $data)
  {
    $this->hydrate($data);
  }

// Hydration
	public function hydrate(array $data)
	{
		foreach ($data as $key => $value) {
			$method = 'set'.ucfirst($key);
			if(method_exists($this, $method))
			{
				$this -> $method($value);
			}
		}
	}  	

// Getters
  public function id()
  {
    return $this->_id;
  }

  public function exhibitorId()
  {
    return $this->_exhibitorId;
  }

  public function addressId()
  {
    return $this->_addressId;
  } 

  public function creationDate()
  {
    return $this->_creationDate;
  } 

// Setters
  public function setId($id)
  {
    $id = (int) $id;
    
    if ($id > 0)
    {
      $this->_id = $id;
    }
  }

  public function setExhibitorId($exhibitorId)
  {
    $exhibManager = new ExhibitorManager();
    $exhibitorId = (int) $exhibitorId;
    
    if ($exhibManager -> exists($exhibitorId)) {
      if ($exhibitorId > 0)
      {
        $this->_exhibitorId = $exhibitorId;
      }
    }
  }

  public function setAddressId($addressId)
  {
    $addressManager = new AddressManager();
    $addressId = (int) $addressId;
    
    if ($addressManager -> exists($addressId)) {
      if ($addressId > 0)
      {
        $this->_addressId = $addressId;
      }
    }
  } 

  public function setCreationDate($creationDate)
  {
	// Conditions to be added
      $this->_creationDate = $creationDate;
  }   
}