<?php 
class AddressManager extends Manager 
{
	  public function exists($info) {
		$db = $this -> connectDb();
	    if (is_numeric($info)) 
	    {    	
	      return (bool) $db->query('SELECT COUNT(*) FROM address WHERE id = '.$info)->fetchColumn();
	    }
	  }

	public function getAddress($input) {
	    if (is_numeric ($input)) {      
			$db = $this -> connectDb();
			$req_user = $db -> prepare('SELECT * FROM address WHERE id = ?');
			$req_user -> execute(array($input));
			$user = $req_user -> fetch();

		    return new User($user);
	    }
	}	

	public function add(Address $address)	{
		$db = $this -> connectDb();

		$q = $db -> prepare('INSERT INTO address(street, postalCode, city, country, creationDate) VALUES(:street, :postalCode, :city, :country, :creationDate, NOW())');

		$q->bindValue(':street', $address->street());
		$q->bindValue(':postalCode', $address->postalCode());
		$q->bindValue(':city', $address->city());
		$q->bindValue(':country', $address->country());
		$q->execute();
	}

	public function update(Address $address) {
		$db = $this -> connectDb();    
		$q = $db->prepare('UPDATE address SET street = :street, postalCode = :postalCode, city = :city, country = :country, privilege = :privilege WHERE id = :id');

	    $q->bindValue(':id', $address->id());
		$q->bindValue(':postalCode', $address->postalCode());
		$q->bindValue(':city', $address->city());
		$q->bindValue(':street', $address->street());
		$q->bindValue(':country', $address->country());

		$q->execute();
	}	
}