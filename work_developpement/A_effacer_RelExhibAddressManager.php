<?php 
class RelExhibAddressManager extends Manager 
{
	public function add(RelExhibAddress $rel)	{
		$db = $this -> connectDb();

		$q = $db -> prepare('INSERT INTO rel_exhibitor_address(exhibitor_id, address_id, creationDate) VALUES(:exhibitor_id, :address_id, NOW())');

		$q->bindValue(':exhibitor_id', $rel->exhibitorId());
		$q->bindValue(':address_id', $rel->addressId());
		$q->execute();
	}

	public function update(RelExhibAddress $rel) {
		$db = $this -> connectDb();    
		$q = $db->prepare('UPDATE rel_exhibitor_address SET exhibitor_id = :exhibitor_id, address_id = :address_id WHERE id = :id');

	    $q->bindValue(':id', $rel->id());
		$q->bindValue(':exhibitor_id', $rel->exhibitor_id());
		$q->bindValue(':address_id', $rel->addressId());
		$q->bindValue(':privilege', $user->privilege());  

		$q->execute();
	}	

	public function getAddressFromExhib(Exhibitor $exhibitor) {
	
		$db = $this -> connectDb();    
		$req_address = $db -> prepare('SELECT address_id FROM rel_exhibitor_address WHERE exhibitor_id = ?');
		$req_address -> execute(array($exhibitor -> id()));
		$address_id = $req_address -> fetch();
	    $addressManager = new AddressManager();
	    // We return the corresponding Exhibitor object
	    return $addressManager -> getAdress($address_id['0']);
	}
}