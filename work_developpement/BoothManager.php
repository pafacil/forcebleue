<?php
class BoothManager extends Manager
{
  public function add(Booth $booth)
  {
    $db = $this -> connectDb();

    $q = $db -> prepare('INSERT INTO booth(exhibitorId, area, shop, comment, content, contentDoc, invitation, pass, creationDate) VALUES(:exhibitorId, :area, :shop, :comment, :content, :contentDoc, :invitation, :pass, NOW())');

    $q->bindValue(':exhibitorId', $booth->exhibitorId());
    $q->bindValue(':area', $booth->area());
    $q->bindValue(':shop', $booth->shop());
    $q->bindValue(':comment', $booth->comment());
    $q->bindValue(':content', $booth->content());
    $q->bindValue(':contentDoc', $booth->contentDoc());
    $q->bindValue(':invitation', $booth->invitation(), PDO::PARAM_INT);
    $q->bindValue(':pass', $booth->pass(), PDO::PARAM_INT);
    $q->execute();
  }

  public function update(Booth $booth)
  {
    $db = $this -> connectDb();    
    $q = $db->prepare('UPDATE booth SET exhibitorId = :exhibitorId, area = :area, shop = :shop, comment = :comment, content = :content, contentDoc = :contentDoc, invitation = :invitation, pass = :pass WHERE id = :id');

    $q->bindValue(':id', $booth->id());
    $q->bindValue(':exhibitorId', $booth->exhibitorId());
    $q->bindValue(':area', $booth->area());
    $q->bindValue(':shop', $booth->shop());
    $q->bindValue(':comment', $booth->comment());
    $q->bindValue(':content', $booth->content());
    $q->bindValue(':contentDoc', $booth->contentDoc());
    $q->bindValue(':invitation', $booth->invitation());
    $q->bindValue(':pass', $booth->pass());
    $q->execute();
  }

  public function delete(Booth $booth)
  {
    $db = $this -> connectDb();

    $db->exec('DELETE FROM booth WHERE id = '.$booth->id());
  }

  public function deleteFromExhibitor(Exhibitor $exhibitor)
  {
    $db = $this -> connectDb();

    $db->exec('DELETE FROM booth WHERE exhibitorId = '.$exhibitor->id());
  }

  public function exists($info) {
  $db = $this -> connectDb();
    if (is_numeric($info)) 
    {     
      return (bool) $db->query('SELECT COUNT(*) FROM booth WHERE id = '.$info)->fetchColumn();
    }
      
    return (bool) false;
  }

  public function getBooth($input)
  {
    if (is_numeric($input)) // If numeric, we search against the id
      {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM booth WHERE id = "'.$input.'"');
      $data = $q -> fetch(PDO::FETCH_ASSOC);

      return new Booth($data);
    }
    return (bool) false;    
  }

  public function getBoothFromExhibitor($input)
  {
    // We confirm that input is numeric
    if (is_numeric($input)) {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM booth WHERE exhibitorId = "'.$input.'"');
      $data = $q -> fetch(PDO::FETCH_ASSOC);

      return new Booth($data);
    }

    return false;
  }

  public function getList()
  {
    $booths = [];

    $db = $this -> connectDb();
    $q = $db -> query('SELECT * FROM booth b
      INNER JOIN exhibitor e ON e.id = b.exhibitorId ORDER BY e.name');

    while ($data = $q->fetch(PDO::FETCH_ASSOC))
    {
      $booths[] = new Booth($data);
    }

    return $booths;
  }
}
