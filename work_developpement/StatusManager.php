<?php
class StatusManager extends Manager
{
  public function add(Status $status)
  {
    $db = $this -> connectDb();

    $q = $db -> prepare('INSERT INTO status(exhibitorId, confirmation, alpha, signed, stand) VALUES(:exhibitorId, :confirmation, :alpha, :signed, :stand)');

    $q->bindValue(':exhibitorId', $status->exhibitorId());
    $q->bindValue(':confirmation', $status->confirmation());
    $q->bindValue(':alpha', $status->alpha());
    $q->bindValue(':signed', $status->signed());
    $q->bindValue(':stand', $status->stand());
    $q->execute();
  }

  public function update(Status $status)
  {
    $db = $this -> connectDb();    
    $q = $db->prepare('UPDATE status SET exhibitorId = :exhibitorId, confirmation = :confirmation, alpha = :alpha, signed = :signed, stand = :stand WHERE id = :id');

    $q->bindValue(':id', $status->id());
    $q->bindValue(':exhibitorId', $status->exhibitorId());
    $q->bindValue(':confirmation', $status->confirmation());
    $q->bindValue(':alpha', $status->alpha());
    $q->bindValue(':signed', $status->signed());
    $q->bindValue(':stand', $status->stand());
    $q->execute();
  }

  public function delete(Status $status)
  {
    $db = $this -> connectDb();

    $db->exec('DELETE FROM status WHERE id = '.$status->id());
  }

  public function deleteFromExhibitor(Exhibitor $exhibitor)
  {
    $db = $this -> connectDb();

    $db->exec('DELETE FROM status WHERE exhibitorId = '.$exhibitor->id());
  }


  public function exists($info) {
  $db = $this -> connectDb();
    if (is_numeric($info)) 
    {     
      return (bool) $db->query('SELECT COUNT(*) FROM status WHERE id = '.$info)->fetchColumn();
    }
      
    return (bool) false;
  }

  public function getStatus($input)
  {
    if (is_numeric($input)) // If numeric, we search against the id
      {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM status WHERE id = "'.$input.'"');
      $data = $q -> fetch(PDO::FETCH_ASSOC);

      return new Status($data);
    }
    return (bool) false;    
  }

  public function getStatusFromExhibitor($input)
  {
    // We confirm that input is numeric
    if (is_numeric($input)) {
      $db = $this -> connectDb();
      $q = $db -> query('SELECT * FROM status WHERE exhibitorId = "'.$input.'"');
      $data = $q -> fetch(PDO::FETCH_ASSOC);

      return new Status($data);
    }

    return false;
  }

  public function getList()
  {
    $status = [];

    $db = $this -> connectDb();
    $q = $db -> query('SELECT * FROM status s
      INNER JOIN exhibitor e ON e.id = s.exhibitorId 
      ORDER BY e.name');

    while ($data = $q->fetch(PDO::FETCH_ASSOC))
    {
      $status[] = $data;
    }

    return $status;
  }
}
