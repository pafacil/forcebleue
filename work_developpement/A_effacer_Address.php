<?php
class Address
{
	private $_id;
  private $_street;
  private $_postalCode;
  private $_city;
	private $_country;
	private $_creationDate;

  public function __construct(array $data)
  {
    $this->hydrate($data);
  }

// Hydration
	public function hydrate(array $data)
	{
		foreach ($data as $key => $value) {
			$method = 'set'.ucfirst($key);
			if(method_exists($this, $method))
			{
				$this -> $method($value);
			}
		}
	}  	

// Getters
  public function id()
  {
    return $this->_id;
  }

  public function street()
  {
    return $this->_street;
  }

  public function postalCode()
  {
    return $this->_postalCode;
  }

  public function city()
  {
    return $this->_city;
  }

  public function country()
  {
    return $this->_country;
  } 
  
  public function creationDate()
  {
    return $this->_creationDate;
  } 

// Setters
  public function setId($id)
  {
    $id = (int) $id;
    
    if ($id > 0)
    {
      $this->_id = $id;
    }
  }

  public function setStreet($street)
  {
    if (is_string($street))
    {
      $this->_street = $street;
    }
  }

  public function setPostalCode($postalCode)
  {
    if (is_numeric($postalCode))
    {
      $this->_postalCode = $postalCode;
    }
  }

  public function setCity($city)
  {
    if (is_string($city))
    {
    	$this->_city = $city;
    }
  }

  public function setCountry($country)
  {
   if (is_string($country))
    {  	
    	$this->_country = $country;
    }
  } 

  public function setCreationDate($creationDate)
  {
	// Conditions to be added
      $this->_creationDate = $creationDate;
  }   
}