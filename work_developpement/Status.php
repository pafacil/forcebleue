<?php

class Status 
{
	private $_id;
  private $_exhibitorId;
	private $_confirmation;
	private $_alpha;
  private $_signed;
  private $_stand;

  public function __construct(array $data)
  {
    $this->hydrate($data);
  }
  
// Hydration
	public function hydrate(array $data)
	{
		foreach ($data as $key => $value) {
			$method = 'set'.ucfirst($key);
			if(method_exists($this, $method))
			{
				$this -> $method($value);
			}
		}
	}


// Getters
  public function id()
  {
    return $this->_id;
  }

  public function exhibitorId()
  {
    return $this->_exhibitorId;
  }

  public function confirmation()
  {
    return $this->_confirmation;
  } 

  public function alpha()
  {
    return $this->_alpha;
  }

  public function signed()
  {
    return $this->_signed;
  } 

  public function stand()
  {
    return $this->_stand;
  } 

  public function creationDate()
  {
    return $this->_creationDate;
  }             

// Setters
  public function setId($id)
  {
    $id = (int) $id;
    
    if ($id > 0)
    {
      $this->_id = $id;
    }
  }

  public function setExhibitorId($exhibitorId)
  {
    $exhibitorId = (int) $exhibitorId;
    
    if ($exhibitorId > 0)
    {
      $this->_exhibitorId = $exhibitorId;
    }
  }

  public function setConfirmation($confirmation)
  {
    if ($confirmation == "0" || $confirmation == "1")
    {
      $this->_confirmation = $confirmation;
    }
  } 

  public function setAlpha($alpha)
  {
    if ($alpha == "0" || $alpha == "1")
    {
      $this->_alpha = $alpha;
    }
  }   

  public function setSigned($signed)
  {
    if ($signed == "0" || $signed == "1")
    {
      $this->_signed = $signed;
    }
  }  

  public function setStand($stand)
  {
    $stand = (int) $stand;
    
    if ($stand > 0)
    {
      $this->_stand = $stand;
    }
  }
}
