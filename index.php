<?php 
 require('controller/controller.php');
 require('controller/user.php');
 require('controller/admin.php');
 require('public/helper/helper.php');
 // require_once('model/Manager.php');

// To load automatically the classes only when needed
function loadClass($class)
{	
	require 'model/'.$class.'.php'; 
}

spl_autoload_register('loadClass');
session_start();	

try {
	// For tests purpose only
	// print_r($_FILES);echo '<br/>';
	// print_r($_POST);echo '<br/>';
	// print_r($_GET);echo '<br/>';	
	// print_r($_SESSION);echo '<br/>';

	// If someone wants to disconnect
	if (isset($_GET['action']) && $_GET['action'] == "logout") {
		session_destroy();
		header('Location: index.php');
	}

	// If there is an existing valid session
	if (isset($_SESSION['activeSession']) && $_SESSION['activeSession'] == 'valid') {
		$user = $_SESSION['user'];

		// For standards users (exhibitors) 
		if ($user -> privilege() == 'user') {
			if (isset($_GET['action']) && $_GET['action'] == "myProfile") {
				userProfile($user);
			} else if (isset($_GET['action']) && $_GET['action'] == "updateProfile") {
				updateProfile($user);
			} else if (isset($_GET['action']) && $_GET['action'] == "myBooth") {
				userBooth($user);
			} else if (isset($_GET['action']) && $_GET['action'] == "updateBooth") {
				updateBooth($user);
			} else if (isset($_GET['action']) && $_GET['action'] == "faq") {
				faq($user);		
			} else if (isset($_GET['action']) && $_GET['action'] == "contactUs") {
				contactUs($user);			
			} else if (isset($_GET['action']) && $_GET['action'] == "contact") {
				contact($user);							
			} else if (isset($_GET['action']) && $_GET['action'] == "getStatus") {
				myStatus($user);									
			} else {				
				mainUser();	
			}
		}
		// For admins	
		else if ($user -> privilege() == 'admin' && !isset($noLoginScreen)) {
			if (isset($_GET['action']) && ($_GET['action'] == "addExhibitor" || $_GET['action'] == "addAdmin")) { 
				addEntryUser();
			} else if (isset($_GET['action']) && $_GET['action'] == "getStatus") {
				listExhibitorsStatus();
			} else if (isset($_GET['action']) && $_GET['action'] == "manage") {
				listActions();
			} else if (isset($_GET['action']) && $_GET['action'] == "modifyStatus") {
				modifyExhibitorsStatus();												
			} else if (isset($_GET['action']) && $_GET['action'] == "updateStatus") {
				updateStatus();	
			} else if (isset($_GET['action']) && $_GET['action'] == "getExhibitor") {
				listExhibitorStatus();	
			} else if (isset($_GET['action']) && $_GET['action'] == "removeExhibitor") {
				removeExhibitor();
			} else if (isset($_GET['action']) && $_GET['action'] == "removeUser") {
				removeUser();								
			} else {
				mainAdmin();
			}
		}				
	}

	else if (isset($_GET['action']) && $_GET['action'] == 'needPasswordChange') {

		if (isset($_SESSION['activeSession']) && $_SESSION['activeSession'] == 'needPasswordChange') {
			$user = $_SESSION['user'];			
			// In case of password update, we check that it's a correct pwd before updating it
				
			if (isset($_GET['option']) && $_GET['option'] == "updatePassword") {
				checkPassword($user);
			}

			// If defaultPassword, we ask the user to change it
			else if ($user -> defaultPassword() == '1' && !isset($noLoginScreen)) {
				loginScreen('changePassword');
			}
		}
	}


	// If someone lost his password
	else if (isset($_GET['action']) && $_GET['action'] == 'forgottenPassword') {
		loginScreen('missingPassword');
	}
	// If someone requested his password
	else if (isset($_GET['action']) && $_GET['action'] == 'sendPassword') {
		sendPassword();
	}

	// Login screen	
	else if ((isset($_GET['action']) && $_GET['action'] == "incorrectCredentials")) {
		loginScreen('');
	} else if ((isset($_GET['action']) && $_GET['action'] == "login")) {
		login();
	} else {
		loginScreen('');
	}

}
catch(Exception $e) {
  $errorMsg = $e->getMessage();
  require('view/errorView.php');  
}
