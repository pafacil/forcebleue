<?php 

function mainUser() {
	require('view/user/userView.php');	
}

function userProfile($user) {
	$exhibManager = new ExhibitorManager();
	$exhibitor = $exhibManager -> getExhibFromUser($user -> id());	

	$mandatoryInformation = [
		'name','type','logo','firstName1', 'lastName1', 'mail1', 'phone1', 'company', 'street', 'postalCode', 'city', 'country', 'contractor', 'vatNumber'
		];

	$emptyMandatoryInformation = [];
	foreach ($mandatoryInformation as $info) {
		if ($exhibitor -> $info() == '') {
			$emptyMandatoryInformation[] = $info;
		} 
	}

	// Variables for form	
	$name = (empty($exhibitor -> name())) ? '...Nom...' : $exhibitor -> name();
	$logo = (empty($exhibitor -> logo())) ? '' : '<br><img src="public/userData/logo/'.$exhibitor -> logo().'" alt="Logo '.$exhibitor -> name().'" style="max-width: 100px; max-height: 100px" >';
	$type = (empty($exhibitor -> type())) ? '...Type...' : $exhibitor -> type();
	$company = (empty($exhibitor -> company())) ? '...Société...' : $exhibitor -> company();
	$street = (empty($exhibitor -> street())) ? '...Nº et rue...' : $exhibitor -> street();
	$postalCode = (empty($exhibitor -> postalCode())) ? '...code postal...' : $exhibitor -> postalCode();
	$city = (empty($exhibitor -> city())) ? '...Ville...' : $exhibitor -> city();
	$country = (empty($exhibitor -> country())) ? '...Pays...' : $exhibitor -> country();
	$contractor = (empty($exhibitor -> contractor())) ? '...Signataire...' : $exhibitor -> contractor();
	$vatNumber = (empty($exhibitor -> vatNumber())) ? '...Numéro de TVA...' : $exhibitor -> vatNumber();	
	$firstName1 = (empty($exhibitor -> firstName1())) ? '...Prénom...' : $exhibitor -> firstName1();
	$lastName1 = (empty($exhibitor -> lastName1())) ? '...Nom...' : $exhibitor -> lastName1();
	$mail1 = (empty($exhibitor -> mail1())) ? '...Mail...' : $exhibitor -> mail1();
	$phone1 = (empty($exhibitor -> phone1())) ? '...Téléphone...' : $exhibitor -> phone1();	
	$firstName2 = (empty($exhibitor -> firstName2())) ? '...Prénom...' : $exhibitor -> firstName2();
	$lastName2 = (empty($exhibitor -> lastName2())) ? '...Nom...' : $exhibitor -> lastName2();
	$mail2 = (empty($exhibitor -> mail2())) ? '...Mail...' : $exhibitor -> mail2();
	$phone2 = (empty($exhibitor -> phone2())) ? '...Téléphone...' : $exhibitor -> phone2();		
	$webUrl = (empty($exhibitor -> webUrl())) ? '...www.notre-site.com...' : $exhibitor -> webUrl();	
	$facebook = (empty($exhibitor -> facebook())) ? '...compte Facebook...' : $exhibitor -> facebook();	
	$twitter = (empty($exhibitor -> twitter())) ? '...compte Twitter...' : $exhibitor -> twitter();

	if (isset($_GET['msg']) && $_GET['msg'] != '') {
		$tempMsg = temporaryMsg(htmlspecialchars($_GET['msg']));
	}

	$page = 'myProfile';
	require('view/user/userProfileView.php');	
}

function updateProfile($user) {
	$exhibManager = new ExhibitorManager();
	$exhibitor = $exhibManager -> getExhibFromUser($user -> id());	

	foreach ($_POST AS $key => $value) {
		if ($value != '' && $value != 'Valider') {
			$method = 'set'.ucfirst($key);
			if(method_exists($exhibitor, $method)) {
				$exhibitor -> $method(trim(htmlspecialchars($value)));
			}
		}
	}

		// Update logo file
	if (isset($_FILES['logo']) && $_FILES['logo']['name'] != '') {
		// Size test
		$maxsize = $_POST['MAX_FILE_SIZE'];

		if (!$_FILES['logo']['error'] > '0') {

			if ($_FILES['logo']['size'] < $maxsize) {
				$extensions_valides = ['jpg', 'jpeg', 'gif', 'png', 'svg', 'ico'];
				$extension_upload = strtolower(substr(strrchr($_FILES['logo']['name'], '.')  ,1)  );
				if (in_array($extension_upload,$extensions_valides)) {
					// Upload and update
					$filename = ($exhibitor -> id()).trim(strtolower((str_replace(' ', '', ($exhibitor -> name()))))).'.'.$extension_upload;
					$fileFinalLocation = 'public/userData/logo/'.$filename;
					$saveFile = move_uploaded_file($_FILES['logo']['tmp_name'],$fileFinalLocation);
					if ($saveFile) $exhibitor -> setLogo($filename);
				} else {
					$tempMsg = "logoUnsupportedType";
				}
			} else {
				$tempMsg = "logoFileTooLarge";
			} 
		} else {
			$tempMsg = "logoTransferError";
		}
	}	

	$exhibManager -> update($exhibitor);

	if (isset($tempMsg)) {
		header('Location: index.php?action=myProfile&msg='.$tempMsg);
	} else {
		header('Location: index.php?action=myProfile&msg=updateOK');
	}
}

function userBooth($user) {
	$exhibManager = new ExhibitorManager();
	$exhibitor = $exhibManager -> getExhibFromUser($user -> id());	
	$packManager = new PackManager();
	$pack = $packManager -> getPackFromExhibitor($exhibitor -> id());	
	$gameManager = new GameManager();
	$games = $gameManager -> getGamesFromExhibitor($exhibitor -> id());
	$hostManager = new HostManager();
	$hosts = $hostManager -> getHostsFromExhibitor($exhibitor -> id());

	// Variables for form	
	$area = (empty($exhibitor -> area())) ? '' : $exhibitor -> area();
	$special = (empty($pack -> special())) ? '0' : $pack -> special();
	$doublePack = (empty($pack -> doublePack())) ? '0' : $pack -> doublePack();
	$standard = (empty($pack -> standard())) ? '0' : $pack -> standard();
	$mini = (empty($pack -> mini())) ? '0' : $pack -> mini();
	$tablePack = (empty($pack -> tablePack())) ? '0' : $pack -> tablePack();
	$association = (empty($pack -> association())) ? '0' : $pack -> association();
	$associationExterieur = (empty($pack -> associationExterieur())) ? '0' : $pack -> associationExterieur();
	$shop = (empty($exhibitor -> shop())) ? '' : $exhibitor -> shop();
	$comment = $exhibitor -> comment();
	$pass = (empty($exhibitor -> pass())) ? '0' : $exhibitor -> pass();
	$invitation = (empty($exhibitor -> invitation())) ? '0' : $exhibitor -> invitation();
	$content = (empty($exhibitor -> content())) ? '' : $exhibitor -> content();
	if (!empty($exhibitor -> contentDoc())) {
		$extension = explode('.',$exhibitor -> contentDoc());
		if ($extension[1] == 'doc' || $extension[1] == 'docx') {
			$icon = 'word.png';
		} else if ($extension[1] == 'odt') {
			$icon = 'odt.png';
		}
		$contentDoc = '
			<div class="existingContentDoc">
				<p><a href="public/userData/contentDoc/'.$exhibitor -> contentDoc().'" download="VotreFichier" class="containerFile containerCentered"><img src="public/images/'.$icon.'" alt="Icone Document" class="imgExistingContentDoc">Contenu actuel</a></p>
				<p>Déposer un nouveau fichier remplace l\'existant</p>
			</div>';			
	} else {
		$contentDoc = '';		
	}

	$tempMsg = temporaryMsg('');

	$page = 'myBooth';
	require('view/user/userBoothView.php');	
}

function updateBooth($user) {
	$exhibManager = new ExhibitorManager();
	$hostManager = new HostManager();
	$packManager = new PackManager();
	$gameManager = new GameManager();

	$exhibitor = $exhibManager -> getExhibFromUser($user -> id());	
	$hosts = $hostManager -> getHostsFromExhibitor($exhibitor -> id());	
	$pack = $packManager -> getPackFromExhibitor($exhibitor -> id());	
	$game = $gameManager -> getGamesFromExhibitor($exhibitor -> id());	

	foreach ($_POST AS $key => $value) {
		if ($value != '' && $value != 'Valider' && !is_array($key)) {
			if ($value == 'on') {$value = '1';}
			$method = 'set'.ucfirst($key);

			if(method_exists($pack, $method)) {
				$pack -> $method(trim(htmlspecialchars($value)));
			}

			if(method_exists($exhibitor, $method)) {
				$exhibitor -> $method(trim(htmlspecialchars($value)));
			}									
		}
	}

	if (!isset($_POST['shop'])) {
		$exhibitor -> setShop('0');	
	}

	if (!empty($_POST['gamesToDelete'])) {
		foreach ($_POST['gamesToDelete'] AS $gameId) {
			if ($gameManager -> exists($gameId)) {
				$game = $gameManager -> getGame($gameId);
				$gameManager -> delete($game);
				}
			}
		}	

	if (!empty($_POST['gamesToAdd'])) {
		foreach ($_POST['gamesToAdd'] AS $gameTitle => $gameClassification) {
			$gameData = [
				'title' => trim(htmlspecialchars(($gameTitle))),
				'classification' => $gameClassification,
				'exhibitorId' => $exhibitor -> id()
			];
			$game = new Game($gameData);

			if (!$gameManager -> isDuplicate($gameTitle, $exhibitor -> id())) {
				$gameManager -> add($game);
			} 				
		}		
	}

	if (!empty($_POST['games'])) {
		foreach ($_POST['games'] AS $game) {
			if ($game['title'] != '' && isset($game['classification']) && $game['classification'] != '') {	
				$title = ucwords(trim(htmlspecialchars($game['title'])));
				$gameData = [
					'title' => $title,
					'classification' => $game['classification'],
					'exhibitorId' => $exhibitor -> id()
				];			
				$game = new Game($gameData);
			
				if (!$gameManager -> isDuplicate($title, $exhibitor -> id())) {
					$gameManager -> add($game);
				} 
			}
		}	
	}

	if (!empty($_POST['hostsToDelete'])) {
		foreach ($_POST['hostsToDelete'] AS $hostId) {
			if ($hostManager -> exists($hostId)) {
				$host = $hostManager -> getHost($hostId);
				$hostManager -> delete($host);
				}
			}
		}	

	if (!empty($_POST['hostsToAdd'])) {
		foreach ($_POST['hostsToAdd'] AS $hostName) {
			$hostData = [
				'name' => trim(htmlspecialchars(($hostName))),
				'exhibitorId' => $exhibitor -> id()
			];
			$host = new Host($hostData);

			if (!$hostManager -> isDuplicate($hostName, $exhibitor -> id())) {
				$hostManager -> add($host);
			} 				
		}		
	}

	if (!empty($_POST['hosts'])) {
		foreach ($_POST['hosts'] AS $host) {
			if ($game['title'] != '' && isset($game['classification']) && $game['classification'] != '') {	
				$title = ucwords(trim(htmlspecialchars($game['title'])));
				$gameData = [
					'title' => $title,
					'classification' => $game['classification'],
					'exhibitorId' => $exhibitor -> id()
				];			
				$game = new Game($gameData);
			
				if (!$gameManager -> isDuplicate($title, $exhibitor -> id())) {
					$gameManager -> add($game);
				} 
			}
		}	
	}

	// Update contentDoc file
	if (isset($_FILES['contentDoc']) && $_FILES['contentDoc']['name'] != '') {
		// Size test
		$maxsize = $_POST['MAX_FILE_SIZE'];

		if (!$_FILES['contentDoc']['error'] > '0') {

			if ($_FILES['contentDoc']['size'] < $maxsize) {
				$extensions_valides = ['doc','docx','odt'];
				$extension_upload = strtolower(substr(strrchr($_FILES['contentDoc']['name'], '.')  ,1)  );
				if (in_array($extension_upload,$extensions_valides)) {
					// Upload and update
					$filename = ($exhibitor -> id()).trim(strtolower((str_replace(' ', '', ($exhibitor -> name()))))).'.'.$extension_upload;
					$fileFinalLocation = 'public/userData/contentDoc/'.$filename;
					$saveFile = move_uploaded_file($_FILES['contentDoc']['tmp_name'],$fileFinalLocation);
					if ($saveFile) $exhibitor -> setContentDoc($filename);
				} else {
					$tempMsg = "contentUnsupportedType";
				}
			} else {
				$tempMsg = "contentFileTooLarge";
			} 
		} else {
			$tempMsg = "contentTransferError";
		}
	}	

	$exhibManager -> update($exhibitor);
	$packManager -> update($pack);

	if (isset($tempMsg)) {
		header('Location: index.php?action=myBooth&msg='.$tempMsg);
	} else {
		header('Location: index.php?action=myBooth&msg=updateOK');
	}
}

function faq($user) {
	$page = 'faq';

	require('view/user/userFaqView.php');	
}

function myStatus($user) {
	$exhibManager = new ExhibitorManager();
	$packManager = new PackManager();
	$hostManager = new HostManager();
	$gameManager = new GameManager();

	$exhibitor = $exhibManager -> getExhibFromUser($user -> id());	
	$hosts = $hostManager -> getHostsFromExhibitor($exhibitor -> id());	
	$pack = $packManager -> getPackFromExhibitor($exhibitor -> id());	
	$game = $gameManager -> getGamesFromExhibitor($exhibitor -> id());	

	$special = (empty($pack -> special())) ? '0' : $pack -> special();
	$doublePack = (empty($pack -> doublePack())) ? '0' : $pack -> doublePack();
	$standard = (empty($pack -> standard())) ? '0' : $pack -> standard();
	$mini = (empty($pack -> mini())) ? '0' : $pack -> mini();
	$tablePack = (empty($pack -> tablePack())) ? '0' : $pack -> tablePack();
	$association = (empty($pack -> association())) ? '0' : $pack -> association();
	$associationExterieur = (empty($pack -> associationExterieur())) ? '0' : $pack -> associationExterieur();
	$pass = (empty($exhibitor -> pass())) ? '0' : $exhibitor -> pass();
	$invitation = (empty($exhibitor -> invitation())) ? '0' : $exhibitor -> invitation();

	if ($exhibitor -> type() == 'editeur') {
		$surface = ($special * 72 + $doublePack * 54 + $standard * 27 + $mini * 9). ' m2' ;
		if ($surface == '0') {$surface = 'Espace partagé';}

		$tables = ($special * 24 + $doublePack * 16 + $standard * 8 + $mini * 3 + $tablePack). ' tables' ;
		if ($tables == '1 tables') {$tables = '1 table';}	

		$chaises = ($tables * 6).' chaises';

		$cout = (($special * 2200 + $doublePack * 1500 + $standard * 800 + $mini *350) + ($pass * 15 + $invitation * 15)).' €';
	
	} else if ($exhibitor -> type() == 'association') {
		$associationPack = ($association == '0') ? 'Aucun': $association.' packs';
		if ($associationPack == '0') {$surface = 'Aucun espace couvert';}

		$surface = ($association * 9). ' m2' ;
		if ($surface == '0') {$surface = 'Aucun espace couvert';}

		$tables = ($association * 3). ' tables' ;
		if ($tables == '1') {$surface = '1 table';}	
		if ($tables == '0') {$surface = '1 table';}	

		$chaises = ($tables * 6).' chaises';

		$associationExterieur = $associationExterieur.' emplacement extérieur';

		$cout = (($association *150) + ($pass * 15) + ($invitation * 15)).' €';
	} else if ($exhibitor -> type() == 'boutique') {
		$cout = ($pass * 15 + $invitation * 15).' € <br>(coût des Pass et Invitations uniquement, voir modalités spécifiques par ailleurs)';
	} else {
		$cout = ($pass * 15 + $invitation * 15).' €';
		if ($cout == '0 €') {$cout = '-';}
	}

	$page = 'myStatus';
	require('view/user/userStatusView.php');	
}

function contactUs($user) {
	$page = 'contactUs';
	require('view/user/userContactView.php');		
}

function contact($user) {
	$exhibManager = new ExhibitorManager();
	$exhibitor = $exhibManager -> getExhibFromUser($user -> id());	

	$expeditor = $user -> mail();
	$expeditorName = $user -> firstName().' '.$user -> lastName();

	$recipient = '"Paris Est Ludique"<pafacil@gmail.com>';

	$resultAssociativeArray = [
		'requete' => "J'ai une requête particulière",
		'boutique' => 'Je suis une boutique, modalités ?',
		'renseignements' => 'Renseignements pratiques',
		'tarifs' => 'À propos des tarifs',
		'autre' => 'Autre',			
	];
	$title = ($_POST['subject'] == '') ? 'Sans titre': trim(ucfirst(htmlspecialchars($_POST['subject'])));
	$subject = ($_POST['type'] != 'null') ? 'Force Bleue : "'.$resultAssociativeArray[$_POST['type']].'"' : 'Force Bleue - Sans sujet';
	$message = '<em>Message de <strong>'.$expeditorName.'</strong></em><br><br>';
	$message .= '"'.trim(nl2br(htmlspecialchars($_POST['message']))).'"';

	if ($_POST['message'] == '' ) {
		$tempMsg = temporaryMsg('emptyMail');
	} else {
		if (sendMail($expeditor,$recipient,$subject,$title,$message)) {
			$tempMsg = temporaryMsg('mailSent');
		} else {
			$tempMsg = temporaryMsg('mailError');			
		}
	}

	$page = 'contactUs';
	require('view/user/userContactView.php');		
}