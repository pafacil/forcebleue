<?php 

function mainAdmin() {
	require('view/admin/adminView.php');
} 

function listActions() {
	$userManager = new UserManager();
	$exhibManager = new ExhibitorManager();	

	require('view/admin/adminManageView.php');
} 

function addEntryUser() {
	$userManager = new UserManager();
	$exhibManager = new ExhibitorManager();
	$packManager = new PackManager();
    
	$inputMail = (isset($_POST['userMail'])) ? trim(htmlspecialchars($_POST['userMail'])) : '';	
	$inputFirstName = (isset($_POST['userFirstName'])) ? ucwords(trim(htmlspecialchars($_POST['userFirstName']))) : '';	
	$inputLastName = (isset($_POST['userLastName'])) ? ucwords(trim(htmlspecialchars($_POST['userLastName']))) : '';    
    $inputExhibitor = (isset($_POST['exhibitorName'])) ? ucwords(trim(htmlspecialchars($_POST['exhibitorName']))) : '';		

    if ($_GET['action'] == "addExhibitor") {
        $addExhib = 1;
        $userPrivilege = 'newUser';
    } else if ($_GET['action'] == "addAdmin") {
        $userPrivilege = 'admin';
        $addAdmin = 1;
    }

    // We check that mandatory values are not empty
	if ((isset($addExhib) && !empty($inputMail) && !empty($inputExhibitor)) OR (isset($addAdmin) && !empty($inputMail) && !empty($inputLastName) && !empty($inputFirstName))) {
		// We check that the mail / name is not already used
		if ((isset($addExhib) && !$userManager -> exists($inputMail) && !$exhibManager -> exists($inputExhibitor)) OR (isset($addAdmin) && !$userManager -> exists($inputMail))) {

			// We set the default password
			$passwords = defaultPassword();
			$defaultPassword = $passwords['0'];
			$hashedPassword = $passwords['1'];

			// We create the related user
			$userData = [
				"mail" => $inputMail,
				"firstName" => $inputFirstName,
				"lastName" => $inputLastName,
				"password" => $hashedPassword,
				"defaultPassword" => true,
				"privilege" => $userPrivilege,
			];
			$newUser = new User($userData);
			$userManager -> add($newUser);			
			$newUser = $userManager -> getUser($inputMail);
			            
            // For new Exhibitor only
            if (isset($addExhib)) {
                // We add an exhibitor entry
                $exhibData = [
                    "name" => $inputExhibitor,
                    "userId" => $newUser -> id(),
                ];
                $exhib = new Exhibitor($exhibData);             
                $exhibManager -> add($exhib);
                $exhib = $exhibManager -> getExhibitor($inputExhibitor);

                // We add a pack entry
                $packData = [
                    "exhibitorId" => $exhib -> id(),
                ];
                $pack = new Pack($packData);
                $packManager -> add($pack);
 
            }

            // If mail needs to be sent
			if (isset($_POST['mailCreation'])) {
                if (isset($addExhib)) {
                    $subject = 'Paris Est Ludique - Votre compte sur le site ForceBleue';
                    $title = 'Exposant';
                    $complement = 'Connectez-vous sur le site <a href="forcebleue.depuisletemps.com">ForceBleue</a>, remplissez votre profil (au moins les champs obligatoires)<br/> puis faites votre demande de stand.';
                } else if (isset($addAdmin)) {
                    $subject = 'Paris Est Ludique - Création de compte Admin';
                    $title = 'Admin';
                    $complement = 'Ce compte vous octroie les privilèges "Admin" sur le site.';
                }
                
                $expeditor = '';
				$recipient = $newUser -> mail();
				$message = '
					Bonjour '.$newUser -> firstName().',<br/><br/>
					Votre compte a été créé sur le site ForceBleue.<br/><br/>
					Votre identifiant / login est : <strong>'.$newUser -> mail().'</strong> .<br/>
					Votre mot de passe est : <strong>'.$defaultPassword.'</strong>.<br/><br/>
					Par sécurité, vous devrez le changer à votre première connexion.
					<br/>	
					'.$complement;
				$header = '';

				$mailTransfer = sendMail($expeditor,$recipient,$subject,$title,$message);
				if ($mailTransfer) {
					$tempMsg = temporaryMsg("createdEntryMailSent");
				} else {
					$tempMsg = temporaryMsg("createdEntryMailFailed");			
				}
				
			} else {
				$tempMsg = temporaryMsg("createdEntry");	
			}
		} else {
			$tempMsg = temporaryMsg("alreadyExisting");
		}
	} else {
		$tempMsg = temporaryMsg("missingField");
	}

	require('view/admin/adminManageView.php');	
}

function listExhibitorsStatus() {
	$exhibManager = new ExhibitorManager();
	$userManager = new UserManager();
	$listExhibitors = $exhibManager -> getList();

	require('view/admin/adminStatusView.php');		
}

function listExhibitorStatus() {
	$exhibManager = new ExhibitorManager();
	$userManager = new UserManager();
	$exhibitorId = (isset($_GET['exhibitor'])) ? htmlspecialchars($_GET['exhibitor']) : '';
	$packManager = new PackManager();
	$gameManager = new GameManager();
	$hostManager = new HostManager();

	if ($exhibManager -> exists($exhibitorId)) {
		$exhibitor = $exhibManager -> getExhibitor($exhibitorId);
		$pack = $packManager -> getPackFromExhibitor($exhibitor -> id());	
		$games = $gameManager -> getGamesFromExhibitor($exhibitor -> id());
		$hosts = $hostManager -> getHostsFromExhibitor($exhibitor -> id());
	}

	if ($exhibitor -> contentDoc() != '') {
		$extension = explode('.',$exhibitor -> contentDoc());
		if ($extension[1] == 'doc' || $extension[1] == 'docx') {
			$icon = 'word.png';
		} else if ($extension[1] == 'odt') {
			$icon = 'odt.png';
		}
		$contentDoc = '
			<div class="existingContentDoc containerFile">
				<a href="public/userData/contentDoc/'.$exhibitor -> contentDoc().'" download="VotreFichier"><div class="containerFile"><img src="public/images/'.$icon.'" alt="Icone Document" class="imgExistingContentDoc">'.$exhibitor -> contentDoc().'</div></a>
			</div>';
	} else {
		$contentDoc = 'Aucun fichier uploadé.';
	}

	require('view/admin/adminExhibitorView.php');		
}

function modifyExhibitorsStatus() {
	$exhibManager = new ExhibitorManager();
	$userManager = new UserManager();
	$listExhibitors = $exhibManager -> getList();

	if (isset($_GET['message']) && $_GET['message'] == 'updateOk') {
		$tempMsg = temporaryMsg("updateOK");
	}

	require('view/admin/adminModifyStatusView.php');		
}

function updateStatus() {
	$userManager = new UserManager();
	$exhibManager = new ExhibitorManager();

	foreach ($_POST as $exhibitorId => $exhibitorStatus) {
		if (is_int($exhibitorId)) {
			(isset($_POST['lineNotToUpdate'])) ? $_POST['lineNotToUpdate'] : $_POST['lineNotToUpdate'] = [];

			if ($exhibitorId != 'exhibitorsStatus' && !(in_array($exhibitorId, $_POST['lineNotToUpdate']))) {
				$exhibitor = $exhibManager -> getExhibitor($exhibitorId);

				if (!array_key_exists('alpha', $exhibitorStatus)) {$exhibitorStatus['alpha'] = '0';}
				if (!array_key_exists('signed', $exhibitorStatus)) {$exhibitorStatus['signed'] = '0';}
				if (!array_key_exists('confirmation', $exhibitorStatus)) {$exhibitorStatus['confirmation'] = '0';}

				foreach ($exhibitorStatus as $key => $value) {
					if ($value == 'on') {$value = '1';}

					$method = 'set'.ucfirst($key);
					if(method_exists($exhibitor, $method))
					{
						$exhibitor -> $method($value);
					}
				}
			$exhibManager -> update($exhibitor);
			}
		}
	}

	header('Location: index.php?action=modifyStatus&message=updateOk');
}

function removeExhibitor() {
	$exhibManager = new ExhibitorManager();
	$exhibitorId = (isset($_POST['exhibitorToDelete'])) ? trim(htmlspecialchars($_POST['exhibitorToDelete'])) : '';

	if ($exhibitorId != '') {
		if ($exhibManager -> exists($exhibitorId)) {			
			$exhibManager -> delete($exhibitorId);

			$tempMsg = temporaryMsg('exhibitorRemoved');
		} else {
			$tempMsg = temporaryMsg('inexistingExhibitor');
		}
	}

	require('view/admin/adminManageView.php');	
}

function removeUser() {
	$userManager = new UserManager();
	$userId = (isset($_POST['userToDelete'])) ? trim(htmlspecialchars($_POST['userToDelete'])) : '';

	if ($userId != '') {
		if ($userManager -> exists($userId)) {			
			$userManager -> delete($userId);

			$tempMsg = temporaryMsg('userRemoved');
		} else {
			$tempMsg = temporaryMsg('inexistingUser');
		}
	}

	require('view/admin/adminManageView.php');	
}