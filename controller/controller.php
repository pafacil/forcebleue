<?php 
// Chargement des classes
// require_once('model/Exhibitor.php');

function loginScreen($option, $pwdMsg = '', $pwdErrors = []) {

	if ($option == 'missingPassword') {
		$warningMessage = '';
		$action = 'index.php?action=sendPassword';
		$passwordFormMsg = '
		<label for="userMail">Utilisateur * </label><input type="email" name="userMail" id="userMail" placeholder="Votre mail..." autofocus required/><br>
		<p><em>Si le compte existe, <br>un mail sera renvoyé à votre adresse <br>avec un mot de passe par défaut</em><p>
		<input type="submit" name="login" id="loginBtn" value="Valider"/>';
		$pwdMsg = '<p class="forgottenPassword"><a href="index.php?">Retour au login</a></p>';
		$java = '';		
	} else if ($option == 'changePassword') {
		$warningMessage = (in_array("differentPwds", $pwdErrors)) ? '<p class=\'red\'>Les deux mots de passe ne sont pas identiques.</p>' : '';		
		$action = 'index.php?action=needPasswordChange&option=updatePassword';
		$newPassword = 1;

		$wrongLength = (in_array("tooShort", $pwdErrors)) ? 'class= "red"' : '';
		$wrongNumber = (in_array("missingNumber", $pwdErrors)) ? 'class= "red"' : '';
		$wrongLowercase = (in_array("missingLowercase", $pwdErrors)) ? 'class= "red"' : '';
		$wrongUppercase = (in_array("missingUppercase", $pwdErrors)) ? 'class= "red"' : '';
		$wrongSpecial = (in_array("missingSpecial", $pwdErrors)) ? 'class= "red"' : '';
		$wrongExtraLength = (in_array("notExtraEnough", $pwdErrors)) ? 'class= "red"' : '';

		$passwordFormMsg = '
		<p><em>Votre mot de passe est celui par défaut.<br>Merci de le changer pour accéder au site.</em></p><br>
		<label for="inputPassword"><strong>Mot de passe</strong></label><input type="password" id="inputPassword" name="password" placeholder="...mot de passe..." autofocus required/><br>
		<label for="inputPasswordBis"><strong>Confirmation</strong></label><input type="password" id="inputPasswordBis" name="passwordBis" placeholder="...le même..." required/><br>
		<input type="submit" name="login" id="loginBtn" class="inactive" value="Mot de passe invalide"/>';

		$pwdMsg = ($pwdMsg == '') ? '<p class="forgottenPassword"><a href="index.php?">Retour</a></p>' : '<p>'.$pwdMsg.'</p>';
		$java = '<script src="public/js/Password.js"></script>';		
	} else {
		$warningMessage = (isset($_GET['action']) && $_GET['action'] == "incorrectCredentials") ? '<p class="red">Utilisateur ou mot de passe incorrect</p>' : '<p></p>';		
		$action = 'index.php?action=login';
		$passwordFormMsg = 
		'<label for="inputMail">Utilisateur</label><input type="email" name="userMail" id="inputMail" placeholder="Votre mail..." autofocus required/><br>
		<label for="inputPwd">Mot de passe</label><input type="password" name="password" id="inputPwd" required/><br>
		<input type="submit" name="login" id="loginBtn" value="Valider"/>';
		$pwdMsg = '<p class="forgottenPassword"><a href="index.php?action=forgottenPassword">Mot de passe oublié</a></p>';
		$java = '';
	}
		
	require('view/loginView.php');
	}

function login() {
    $userManager = new UserManager();

	$inputMail = (isset($_POST['userMail'])) ? htmlspecialchars($_POST['userMail']) : '';
	$password = (isset($_POST['password'])) ? htmlspecialchars($_POST['password']) : '';
	// We check if user exists
	if ($userManager -> exists($inputMail)) {
		// If user exists, we create the related object
		$user = $userManager -> getUser($inputMail);
		// We check that the password is correct
		if ($userManager -> checkPassword($inputMail, $password)) {
			// If correct, we enable the session and create session variables
			$_SESSION['user'] = $user;
			if ($user -> defaultPassword() == '0') {
				$_SESSION['activeSession'] = 'valid';
				header('Location: index.php');
			} else if ($user -> defaultPassword() == '1') {
				$_SESSION['activeSession'] = 'needPasswordChange';
				header('Location: index.php?action=needPasswordChange');
			} else {
				header('Location: index.php');
			}
		} else {
			// If any error, we redirect the user to the login screen
			header('Location: index.php?action=incorrectCredentials');
		}
	} else {
		// If any error, we redirect the user to the login screen
		header('Location: index.php?access=adminblog&action=incorrectCredentials');
	}
}

function checkPassword($user) {
	$userManager = new UserManager();

	$password = trim(htmlspecialchars($_POST['password']));
	$passwordBis = trim(htmlspecialchars($_POST['passwordBis']));

	if ($passwordBis != $password) {
		$errorPwds[] = 'differentPwds';
		$errorPwds[] = 'badPwd';
	} else {
		$errorPwds = isValidPassword($password);
	}

	if (!in_array('badPwd',$errorPwds)) {
		// Password is good, we update it
		$hashedPassword = password_hash($password, PASSWORD_DEFAULT);	
		$user -> setPassword($hashedPassword);
		$user -> setDefaultPassword('0');
		
		if ($user -> privilege() == 'newUser') {
			$user -> setPrivilege('user');
		}

		$userManager -> update($user);
		$_SESSION['activeSession'] = 'valid';
		header('Location: index.php');
	} else {
		loginScreen('changePassword','',$errorPwds);
	}
}

function sendPassword() {
    $userManager = new UserManager();
	$inputMail = (isset($_POST['userMail'])) ? htmlspecialchars($_POST['userMail']) : '';

	if ($userManager -> exists($inputMail)) {
		// If user exists, we create the related object
		$user = $userManager -> getUser($inputMail);

		// We create a default password 
		$defaultPasswords = defaultPassword($user, $userManager);
		$defaultPassword = $defaultPasswords['0'];
		$hashedPassword = $defaultPasswords['1'];

		// We update the db
		$user -> setPassword($hashedPassword);
		$user -> setDefaultPassword('1');
		$userManager -> update($user);

		// We send the default password
		$recipient = $user -> mail();
		$subject = 'ForceBleue - Reset de votre mot de passe';
		$title = 'Reset de votre mot de passe';
		$message = '
				Bonjour '.$user -> firstName().',<br/><br/>
				Suite à votre demande, votre mot de passe a été réinitialisé.<br/><br/>
				Pour rappel, votre identifiant / login est : <strong>'.$user -> mail().'</strong> .<br/>
				Votre mot de passe est : <strong>'.$defaultPassword.'</strong>.<br/>
				Par sécurité, vous devrez le changer à votre prochaine connexion.';

		$expeditor = '';

		sendMail($expeditor,$recipient,$subject,$title,$message);
	}

	header('Location: index.php');
}
