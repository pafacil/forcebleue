<?php
$title = 'ForceBleue - Nous Contacter'; 
$head_options = ''; 

ob_start(); ?>
<div id="contact">
	<p><? $tempMsg ?></p>
	<h1>Nous contacter</h1>
	<form action="index.php?action=contact" method="post" class="containerColumn">
		<?php 
		selection('question','1','single','À propos de...','','90%','type',''); ?>
		<br>		
		<input type="text" name="subject" placeholder="Sujet..."/><br>
		<textarea name="message" placeholder="Mon message..." rows="10"></textarea><br>
		<input type="submit" name="contact" value="Envoyer"/>		
	</form>
	<p><em>Vous pouvez aussi nous envoyer un mail directement à <a href="mailto:?to=contact@parisestludique.fr&subject=Site%20ForceBleue%20:%20renseignements">contact@parisestludique.fr</a></em></p>
</div>
<?php $content = ob_get_clean(); 	

ob_start(); 
$java = ''; 

require('templateUser.php'); ?>