<?php
$title = 'ForceBleue - FAQ'; 
$head_options = ''; 

ob_start(); ?>
<div id="faq">
	<h1>FAQ</h1>
	<p>Tout ce que vous avez toujours voulu savoir sur PEL sans jamais osé le demander.</p>
	<div class="pop">
		<h2 class="popTitle container"><span class="fas fa-caret-down"></span>
			<img src="public/images/question.png" alt="Question" class="question">
			Pass ou Invitation ?
		</h2>
		<div class="popContent">
			<p>Un <strong>pass</strong> est une entrée pour l'un de vos collaborateurs. Elle est nécessaire pour que celui-ci puisse entrer dans l'enceinte du festival.</p>
			<p>Une <strong>invitation</strong> est destinée à offrir à qui bon vous semble.</p>
		</div>
	</div>
	<div class="pop">
		<h2 class="popTitle container"><span class="fas fa-caret-down"></span>
			<img src="public/images/question.png" alt="Question" class="question">
			Vente ou pas sur mon stand
		</h2>
		<div class="popContent">
			<p>Dans la page consacrée à votre stand, vous pouvez indiquer si vous souhaitez faire (un peu) de vente sur votre stand. N'oubliez pas que l'activité principale doit rester la présentation et la découverte des jeux au public, qui est la vocation de notre festival.</p>
			<p>Par ailleurs, sachez que la mairie est en droit de réclamer des droits si elle considère que votre activité correspond plus à de la vente qu'à de l'exposition. Nous vous facturerions alors ses droits.</p>
			<p>Il y a des boutiques sur le site, n'hésitez pas à vous mettre en relation avec elles pour qu'elles ne manquent de rien.</p>
		</div>
	</div>
	<div class="pop">
		<h2 class="popTitle container"><span class="fas fa-caret-down"></span>
			<img src="public/images/question.png" alt="Question" class="question">
			Parking
		</h2>
		<div class="popContent">
			<p>Pour l'instant, nous ne disposons pas de droit au parking (comme chaque année, nous essaierons une nouvelle tentative avec la préfecture mais la précédente n'a pas été couronnée de succcès).</p>
		</div>
	</div>
	<div class="pop">
		<h2 class="popTitle container"><span class="fas fa-caret-down"></span>
			<img src="public/images/question.png" alt="Question" class="question">
			Je ne trouve pas la réponse à ma question
		</h2>
		<div class="popContent">
			<p>Dans ce cas, n'hésitez pas à nous joindre à <strong><a href="mailto:?to=contact@parisestludique.fr&subject=Demande%20de%20Renseignements">contact@parisestludique.fr</a></strong> ou à remplir le questionnaire à partir du menu Contacts".</p>
		</div>
	</div>	
</div>
<?php $content = ob_get_clean(); 		

ob_start(); 
$java = ''; 

require('templateUser.php'); ?>