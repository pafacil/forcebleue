<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
	   	<meta name="description" content="ForceBleue est le site dédié aux exposants de Paris Est Ludique. Il nous permet de référencer le statut d'avancement des contrats avec chaque éditeur, boutique ou association." />  
	   	<title><?= $title ?></title>
	    <link href="public/css/style.css" rel="stylesheet" /> 
	    <link href="public/chosen/chosen.css" rel="stylesheet" /> 
	    <link rel="icon" type="image/x-icon" href="public/images/favicon.png" />
	    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet"> 
	    <?= $head_options ?>
	</head>
	<body>
		<header>
 			<div id="headerContent">
				<div id="logout" onclick="document.location='index.php?action=logout'">Déconnexion</div>
				<div id="logoutS" onclick="document.location='index.php?action=logout'"><img src="public/images/logout.png" alt="Logout" title="Déconnexion"></div>				
				<img src="public/images/pelLogo.png" id="pelLogo" alt="Logo PEL">

				<nav id="menu" class="user">
					<div class="pop">
						<h2 class="popTitle popTitleRequest"><div class="openMenuBtn menuS" id="openMenuBtn"><img src="public/images/menuTop.png" alt="Menu" title="Ouvrir menu" class="imgMenuS menuTop"><img src="public/images/menuMiddle.png" alt="Menu" title="Ouvrir menu" class="imgMenuS menuMiddle"><img src="public/images/menuBottom.png" alt="Menu" title="Ouvrir menu" class="imgMenuS menuBottom"></div></h2>
						<div class="popContent ">
							<ul>
								<li><a href="index.php">Accueil</a></li>
								<li><a href="index.php?action=myProfile">Profil</a></li>
								<li><a href="index.php?action=myBooth">Stand</a></li>
								<li><a href="index.php?action=getStatus">Récap</a></li>
								<li><a href="index.php?action=faq">FAQ</a></li>
								<li><a href="index.php?action=contactUs">Contacts</a></li>
							</ul>
						</div>
					</div>
				</nav>
			</div> 
		</header>

		<span class="arrowToTop"></span>
	    <?php if (isset($tempMsg)) { ?>
			<div id="tempMessage">
				<?= $tempMsg ?>        	
			</div>
		<?php } ?>
		
		<section id="content">
	    	<?= $content ?>
		</section>

		<footer>
 			<div id="footerContent">
				<img src="public/images/forceBleueLogo.png" alt="Logo Force Bleue">
				<div id="footerContent">
		            © Copyright - Tous droits réservés - 2018          
		        </div>
		    </div>
		</footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<?= $java ?>
	    <script src="public/js/Pops.js"></script>
	    <script src="public/js/main.js"></script>	
	</body>
</html>