<?php
$title = 'ForceBleue - Récapitulatif'; 
$head_options = ''; 

ob_start(); ?>
<div id="myStatus">
	<h1>Récapitulatif</h1>
	<div class="pop">
		<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-up"></span>Pack</h2>
		<div class="popContent container open">
			<table>
				<?php if ($exhibitor -> type() == 'editeur') { ?>
				<tr>
					<th>Spécial (2200€)</th>
					<td class="numberBig"><?= $special ?></td>
				</tr>		
				<tr>
					<th>Double (1500€)</th>
					<td class="numberBig"><?= $doublePack ?></td>
				</tr>	
				<tr>
					<th>Standard (800€)</th>
					<td class="numberBig"><?= $standard ?></td>
				</tr>	
				<tr>
					<th>Mini (350€)</th>
					<td class="numberBig"><?= $mini ?></td>
				</tr>
				<tr>
					<th>Table (150€)</th>
					<td class="numberBig"><?= $tablePack ?></td>
				</tr>
				<tr>
					<th>Surface couverte</th>
					<td class="numberBig"><?= $surface ?></td>
				</tr>
				<tr>
					<th>Tables</th>
					<td class="numberBig"><?= $tables ?></td>
				</tr>
				<tr>
					<th>Chaises</th>
					<td class="numberBig"><?= $chaises ?></td>
				</tr>					
				<?php } else if ($exhibitor -> type() == 'association')	{ ?> 
					<tr>
						<th>Espace couvert (150€)</th>
						<td class="numberBig"><?= $associationPack ?></td>
					</tr>
					<?php if ($surface != "Aucun espace couvert") { ?>
						<tr>
							<th>Tables</th>
							<td class="numberBig"><?= $tables ?></td>
						</tr>
						<tr>
							<th>Chaises</th>
							<td class="numberBig"><?= $chaises ?></td>
						</tr>
					<?php } ?>
					<tr>
						<th>Espace extérieur (gratuit)</th>
						<td class="numberBig"><?= $associationExterieur ?></td>
					</tr>				
				<?php }	else if ($exhibitor -> type() == 'boutique') { ?>
					<tr>
						<td>Pour les boutiques, modalités à voir par email.<br>
						Merci de nous contacter directement à contact@pel.fr (ou par l'onglet Nous Contacter).</td>
					</tr>
				<?php }	else { ?>
					<tr>
						<td>Avant de pouvoir voir le récapitulatif de votre demande<br>
						Vous devez déjà définir le type de votre stand dans Mes Infos.</td>
					</tr>
				<?php } ?>
			</table>
		</div>
	</div>

	<div class="pop">
		<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-up"></span>Entrées</h2>
		<div class="popContent container open">
			<table>
				<tr>
					<th>Pass (15€)</th>
					<td class="numberBig"><?= $pass ?></td>
				</tr>
				<tr>
					<th>Invitation (15€)</th>
					<td class="numberBig"><?= $invitation ?></td>
				</tr>				
			</table>
		</div>
	</div>

	<h1>Coût total : <strong><?php echo (isset($cout)) ? $cout : '-'; ?></strong></h1>		    	    

</div>
<?php $content = ob_get_clean(); 	

ob_start(); 
$java = ''; 

require('templateUser.php'); ?>