<?php
$title = 'ForceBleue - Mes Infos'; 
$head_options = ''; 

ob_start(); ?>
<div id="profile">
	<form action="index.php?action=updateProfile" method="post" enctype="multipart/form-data">
		<!-- Max size set for Logo file (20 Mo) -->
		<input type="hidden" name="MAX_FILE_SIZE" value="20971520" />

		<h1 class="info">Mes infos - <?= $name ?></h1>

		<?php if (!empty($emptyMandatoryInformation)) { ?>
			<h2 class="red paddingBottom">Informations obligatoires non renseignées en rouge.</h2>
		<?php } ?>

		<div class="pop">
			<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-up"></span><span <?php if (in_array('type', $emptyMandatoryInformation) || in_array('logo', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Qui suis-je ?</span></h2>
			<div class="popContent container containerType">		
				<div class='flexContent flexContentType'>	        
					<input type="radio" name="type" id="association" value="association" <?php if ($type == "association") {echo 'checked';} ?>>
	    			<label for="association" class="type asso"></label>
	    			<input type="radio" name="type" id="boutique" value="boutique" <?php if ($type == "boutique") {echo 'checked';} ?>>
	    			<label for="boutique" class="type shop"></label>
	    			<input type="radio" name="type" id="editeur" value="editeur" <?php if ($type == "editeur") {echo 'checked';} ?>>
	    			<label for="editeur" class="type publisher"></label>
				</div>				
				<div class='flexContent flexContentLogo'>
					<img src="public/<?php if (in_array('logo', $emptyMandatoryInformation)) {echo 'images/yourLogo.png';} else {echo 'userData/logo/'.$exhibitor -> logo();}?>" alt="Pancarte Votre Logo" class="<?php if (in_array('logo', $emptyMandatoryInformation)) {echo 'yourLogo';} else {echo 'logo';} ?>"><br>
					<input type="file" id="upload" class="upload" name="logo"/>
					<label for="upload" id="labelUpload">
						<img width="64" height="64" class="uploadIcon" title="Cliquez pour uploader votre logo" alt="Upload Logo" src="public/images/upload.png" />
						<div class="uploadFilename"><em>Fichiers jpg/jpeg/png/gif/ico/svg</em></div>
					</label>
				</div>
			</div>
		</div>
		<div class="pop">
			<h2 class="popTitle popTitleRequest <?php if (in_array('firstName1', $emptyMandatoryInformation) || in_array('lastName1', $emptyMandatoryInformation) || in_array('mail1', $emptyMandatoryInformation) || in_array('phone1', $emptyMandatoryInformation)) {echo 'red"';}?>"><span class="fas fa-caret-up"></span>Contact Principal</h2>
			<div class="popContent container ">	
				<table>
					<tr>
						<th><label for= 'firstName1' <?php if (in_array('firstName1', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Prénom</label></th>
						<td><input type="text" name="firstName1" placeholder='<?= $firstName1 ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'lastName1' <?php if (in_array('lastName1', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Nom</label></th>
						<td><input type="text" name="lastName1" placeholder='<?= $lastName1 ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'mail1' <?php if (in_array('mail1', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Mail</label></th>
						<td><input type="mail" name="mail1" placeholder='<?= $mail1 ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'phone1' <?php if (in_array('phone1', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Téléphone</label></th>
						<td><input type="text" name="phone1" placeholder='<?= $phone1 ?>'/></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="pop">
			<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-up"></span>Contact Secondaire</h2>
			<div class="popContent container ">
				<table>
					<tr>
						<th><label for= 'firstName2' <?php if (in_array('firstName2', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Prénom</label></th>
						<td><input type="text" name="firstName2" placeholder='<?= $firstName2 ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'lastName2' <?php if (in_array('lastName2', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Nom</label></th>
						<td><input type="text" name="lastName2" placeholder='<?= $lastName2 ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'mail2' <?php if (in_array('mail2', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Mail</label></th>
						<td><input type="mail" name="mail2" placeholder='<?= $mail2 ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'phone2' <?php if (in_array('phone2', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Téléphone</label></th>
						<td><input type="text" name="phone2" placeholder='<?= $phone2 ?>'/></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="pop">
			<h2 class="popTitle popTitleRequest <?php if (in_array('company', $emptyMandatoryInformation) || in_array('street', $emptyMandatoryInformation) || in_array('postalCode', $emptyMandatoryInformation) || in_array('city', $emptyMandatoryInformation) || in_array('country', $emptyMandatoryInformation) || in_array('contractor', $emptyMandatoryInformation) || in_array('vatNumber', $emptyMandatoryInformation) ) {echo 'red"';}?>"><span class="fas fa-caret-up"></span>Données Facturation</h2>
			<div class="popContent container ">
				<table>
					<tr>
						<th><label for= 'company'  <?php if (in_array('company', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Nom de la société</label></th>
						<td><input type="text" name="company" placeholder='<?= $company ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'street' <?php if (in_array('street', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Adresse (nº et rue)</label></th>
						<td><input type="text" name="street" placeholder='<?= $street ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'postalCode' <?php if (in_array('postalCode', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Code postal</label></th>
						<td><input type="number" name="postalCode" placeholder='<?= $postalCode ?>'/><br></td>
					</tr>
					<tr>
						<th><label for= 'city' <?php if (in_array('city', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Ville</label></th>
						<td><input type="text" name="city" placeholder='<?= $city ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'country' <?php if (in_array('country', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Pays</label></th>
						<td><input type="text" name="country" placeholder='<?= $country ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'contractor' <?php if (in_array('contractor', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Signataire</label></th>
						<td><input type="text" name="contractor" placeholder='<?= $contractor ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'vatNumber' <?php if (in_array('vatNumber', $emptyMandatoryInformation)) {echo 'class = "red"';}?>>Numéro de TVA</label></th>
						<td><input type="number" name="vatNumber" placeholder='<?= $vatNumber ?>'/></td>
					</tr>
				</table>
			</div>
		</div>    
		<div class="pop">
			<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-up"></span>Communication</h2>	
			<div class="popContent container ">
				<table>
					<tr>
						<th><label for= 'webUrl'>Mon site web</label></th>
						<td><input type="text" name="webUrl" placeholder='<?= $webUrl ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'facebook'>Mon compte Facebook</label></th>
						<td><input type="text" name="facebook" placeholder='<?= $facebook ?>'/></td>
					</tr>
					<tr>
						<th><label for= 'twitter'>Mon compte Twitter</label></th>
						<td><input type="text" name="twitter" placeholder='<?= $twitter ?>'/></td>
					</tr>
				</table>	
			</div>
		</div>
		<input type="submit" name="myProfile" value="Valider" id="submitBtn"/>
	</form>
</div>
<?php $content = ob_get_clean(); 

ob_start(); ?>
	    <script src="public/js/FixedSubmitBtn.js"></script>
<?php $java = ob_get_clean(); 

require('templateUser.php'); ?>