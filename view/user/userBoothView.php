<?php
$title = 'ForceBleue - Mon Stand'; 
$head_options = ''; 

ob_start(); ?>
<div id="booth">
	<form action="index.php?action=updateBooth" method="post" enctype="multipart/form-data" id ="myBooth">
		<!-- Max size set for Logo file (20 Mo) -->
			<input type="hidden" name="MAX_FILE_SIZE" value="20971520" />

		<h1>Mon stand</h1>
	
		<div class="pop">
			<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-down"></span>Espaces</h2>
			<div class="popContent container containerCentered area">

			<?php if (($exhibitor -> type()) == "editeur") { ?>
		        <input type="radio" name="area" id="village" value="village" <?php if ($area == "village") {echo 'checked';} ?>>
		        <label for="village" class='area village'></label>
		        <input type="radio" name="area" id="alice" value="alice" <?php if ($area == "alice") {echo 'checked';} ?>>
		        <label for="alice" class='area alice'></label>
		        <input type="radio" name="area" id="figurine" value="figurine" <?php if ($area == "figurine") {echo 'checked';} ?>>
		        <label for="figurine" class='area figurine'></label>
		        <input type="radio" name="area" id="role" value="role" <?php if ($area == "role") {echo 'checked';} ?>>
		        <label for="role" class='area role'></label>

			<?php } else if (($exhibitor -> type()) == "association") { ?>
		        <input type="radio" name="area" id="association" value="association" <?php if ($area == "association" || $exhibitor -> type() == 'association') {echo 'checked';} ?>>
		        <label for="association" class='area assoBooth'></label>

			<?php } else if (($exhibitor -> type()) == "boutique") { ?>
		        <input type="radio" name="area" id="boutique" value="boutique" <?php if ($area == "boutique" || $exhibitor -> type() == 'boutique') {echo 'checked';} ?>>
		        <label for="boutique" class='area shopBooth'></label>		        
			<?php } else {
				echo 'Vous devez spécifier le type d\'Exposant dans "Mes Infos"<br>
					avant de pouvoir choisir votre Espace.';
			}?>
		    </div>
		</div>


		<div class="pop">
			<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-down"></span>Packs</h2>
			<div class="popContent">

			<?php if (($exhibitor -> type()) == "editeur") { ?>
				<div class="packList">
					<div class="pack" id="special">
						<div class="description">Spécial</div>
						<table>
							<tr class="blueSky">
								<td>24</td>
								<td> x</td>
								<td><img src="public/images/table.png" alt="Table"></td>
							</tr>
							<tr class="blueSky">
								<td>72</td>
								<td> m<sup>2</sup></td>
								<td><img src="public/images/surface.png" alt="Surface"></td>
							</tr>
							<tr class="blueSky">
								<td>2200</td>
								<td> €</td>
								<td><img src="public/images/price.png" alt="Prix"></td>
							</tr>
						</table>
						<div class="choice">
							<div class="less" id="specialLess"><img src="public/images/remove50.png" class="btnPlusMinus" alt="Ajouter"></div>
							<div id="specialNumber"><?= $special ?></div>
							<div class="more" id="specialMore"><img src="public/images/add50.png" class="btnPlusMinus" alt="Retrancher"></div>
						</div>								
					</div>	
					<div class="pack" id="doublePack">
						<div class="description">Double</div>
						<table>
							<tr class="blueSky">
								<td>16</td>
								<td> x</td>
								<td><img src="public/images/table.png" alt="Table"></td>
							</tr>
							<tr class="blueSky">
								<td>54</td>
								<td> m<sup>2</sup></td>
								<td><img src="public/images/surface.png" alt="Surface"></td>
							</tr>
							<tr class="blueSky">
								<td>1500</td>
								<td> €</td>
								<td><img src="public/images/price.png" alt="Prix"></td>
							</tr>
						</table>
						<div class="choice">
							<div class="less" id="doublePackLess"><img src="public/images/remove50.png" class="btnPlusMinus" alt="Ajouter"></div>
							<div id="doublePackNumber"><?= $doublePack ?></div>
							<div class="more" id="doublePackMore"><img src="public/images/add50.png" class="btnPlusMinus" alt="Retrancher"></div>
						</div>								
					</div>	
					<div class="pack" id="standard">
						<div class="description">Standard</div>
						<table>
							<tr class="blueSky">
								<td>8</td>
								<td> x</td>
								<td><img src="public/images/table.png" alt="Table"></td>
							</tr>
							<tr class="blueSky">
								<td>27</td>
								<td> m<sup>2</sup></td>
								<td><img src="public/images/surface.png" alt="Surface"></td>
							</tr>
							<tr class="blueSky">
								<td>800</td>
								<td> €</td>
								<td><img src="public/images/price.png" alt="Prix"></td>
							</tr>
						</table>
						<div class="choice">
							<div class="less" id="standardLess"><img src="public/images/remove50.png" class="btnPlusMinus" alt="Ajouter"></div>
							<div id="standardNumber"><?= $standard ?></div>
							<div class="more" id="standardMore"><img src="public/images/add50.png" class="btnPlusMinus" alt="Retrancher"></div>
						</div>								
					</div>	
					<div class="pack" id="mini">
						<div class="description">Mini</div>
						<table>
							<tr class="blueSky">
								<td>3</td>
								<td> x</td>
								<td><img src="public/images/table.png" alt="Table"></td>
							</tr>
							<tr class="blueSky">
								<td>9</td>
								<td> m<sup>2</sup></td>
								<td><img src="public/images/surface.png" alt="Surface"></td>
							</tr>
							<tr class="blueSky">
								<td>350</td>
								<td> €</td>
								<td><img src="public/images/price.png" alt="Prix"></td>
							</tr>
						</table>
						<div class="choice">
							<div class="less" id="miniLess"><img src="public/images/remove50.png" class="btnPlusMinus" alt="Ajouter"></div>
							<div id="miniNumber"><?= $mini ?></div>
							<div class="more" id="miniMore"><img src="public/images/add50.png" class="btnPlusMinus" alt="Retrancher"></div>
						</div>								
					</div>
					<div class="pack" id="tablePack">
						<div class="description">Table</div>
						<table>
							<tr class="blueSky">
								<td>1</td>
								<td> x</td>
								<td><img src="public/images/table.png" alt="Table"></td>
							</tr>
							<tr class="blueSky">
								<td>3</td>
								<td> m<sup>2</sup></td>
								<td><img src="public/images/surface.png" alt="Surface"></td>
							</tr>
							<tr class="blueSky">
								<td>150</td>
								<td> €</td>
								<td><img src="public/images/price.png" alt="Prix"></td>
							</tr>
						</table>
						<div class="choice">
							<div class="less" id="tablePackLess"><img src="public/images/remove50.png" class="btnPlusMinus" alt="Ajouter"></div>
							<div id="tablePackNumber"><?= $mini ?></div>
							<div class="more" id="tablePackMore"><img src="public/images/add50.png" class="btnPlusMinus" alt="Retrancher"></div>
						</div>								
					</div>									
				</div>											
			<?php } else if (($exhibitor -> type()) == "association") { ?>
				<div class="packList">
					<div class="pack" id="association">
						<div class="description">Association</div>
						<table>
							<tr class="blueSky">
								<td>3</td>
								<td> x</td>
								<td><img src="public/images/table.png" alt="Table"></td>
							</tr>
							<tr class="blueSky">
								<td>9</td>
								<td> m<sup>2</sup></td>
								<td><img src="public/images/surface.png" alt="Surface"></td>
							</tr>
							<tr class="blueSky">
								<td>150</td>
								<td> €</td>
								<td><img src="public/images/price.png" alt="Prix"></td>
							</tr>
						</table>
						<div class="choice">
							<div class="less" id="associationLess"><img src="public/images/remove50.png" class="btnPlusMinus" alt="Ajouter"></div>
							<div id="associationNumber"><?= $mini ?></div>
							<div class="more" id="associationMore"><img src="public/images/add50.png" class="btnPlusMinus" alt="Retrancher"></div>
						</div>								
					</div>
					<div class="pack" id="associationExterieur">
						<div class="description">Extérieur</div>
						<table>
							<tr class="blueSky">
								<td>Activité</td>
							</tr>
							<tr class="blueSky">
								<td>Extérieure</td>
							</tr>
							<tr class="blueSky">
								<td>Gratuit !</td>
							</tr>
						</table>
						<div class="choice">
							<div class="less" id="associationExterieurLess"><img src="public/images/remove50.png" class="btnPlusMinus" alt="Ajouter"></div>
							<div id="associationExterieurNumber"><?= $mini ?></div>
							<div class="more" id="associationExterieurMore"><img src="public/images/add50.png" class="btnPlusMinus" alt="Retrancher"></div>
						</div>								
					</div>	
				</div>		
			<?php } else if (($exhibitor -> type()) == "boutique") { ?>
				<h2 class="packShop">En tant que boutique, merci de nous contacter à contact@pel.fr pour discuter des conditions et de la mise en place de votre espace.</h2>
			<?php } else {
				echo 'Vous devez spécifier le type d\'Exposant dans "Mes Infos"<br>
					avant de pouvoir choisir votre Pack.';
			}?>
			</div>
		</div>
		<div class="pop">
			<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-down"></span>Entrées</h2>
			<div class="popContent ">
				<table>
					<tr>
						<th><label for= 'pass'>Pass exposants (15€)</label></th>
						<td><input type="number" name="pass" placeholder='<?= $pass ?>'></td>
					</tr>				
					<tr>
						<th><label for= 'invitation'>Invitations (15€)</label></th>
						<td><input type="number" name="invitation" placeholder='<?= $invitation ?>'></td>
					</tr>
				</table>		
			</div>
		</div>
		
		<div class="pop <?php if (($exhibitor -> type()) == "boutique") {echo ' hidden';} ?>">
			<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-down"></span>Vente</h2>
			<div class="popContent ">
				<table>
					<tr>
						<th><label for= 'shop'>Je vends sur mon stand</label></th>
						<td>				
							<div class="flip-container  <?php if ($shop == '1') {echo 'flip';} ?>">
								<div class="flipper">
									<div class="koB" title="Non">
									</div>
									<div class="okB" title="Oui">
									</div>
								</div>
							</div>
							<input type="checkbox" name="shop" id="shop" class="hidden" <?php if ($shop == '1') {echo 'checked';} ?>>
						</td>
					</tr>				
				</table>		
			</div>
		</div>		
		<div class="pop <?php if (($exhibitor -> type()) == "boutique") {echo ' hidden';} ?>">
			<h2 class="popTitle popTitleRequest popTitleGame"><span class="fas fa-caret-down"></span>Jeux présentés sur le stand</h2>
			<div class="popContent popContentGame">
				<div class="newGame container containerCentered jeux" id="addGameLine">
					<input type="text" id="newGameTitle" name="games" placeholder="...nom...">
					<div class="container" >
						<input type="radio" name="newGameClassification" id="classic" value="classic">
		    			<label for="classic" class="classification classic"></label>
		    			<input type="radio" name="newGameClassification" id="new" value="new">
		    			<label for="new" class="classification new"></label>
		    			<input type="radio" name="newGameClassification" id="proto" value="proto">
		    			<label for="proto" class="classification proto"></label>
						<img src="public/images/add.png" alt="Bouton Ajouter Jeu" id="addGameBtn" class="gameToBeAdded hostIcon">
					</div>					
				</div>
				<div id="ourGames">						
					<?php
					$classificationNames = [
						"classic" => "Classique",
						"new" => "Nouveauté",
						"proto" => "Proto-Projet"
					];

					 if(!empty($games)) { 
						foreach ($games as $game) { ?>
						<div id="<?= 'game'.$game -> id() ?>" class="gameList">
							<?= $game -> title() .' ('.$classificationNames[$game -> classification()].')' ?>
							<img src="public/images/koS.png" id='<?= $game -> id() ?>' alt="Bouton Retirer Hôte" class="hostIcon gameToBeDeleted">
						</div>
						<?php }
					} ?>
				</div>				
			</div>
		</div>
		<div class="pop <?php if (($exhibitor -> type()) == "boutique") {echo ' hidden';} ?>">
			<h2 class="popTitle popTitleRequest popTitleHost"><span class="fas fa-caret-down"></span>Acteurs ludiques hébergés</h2>
			<div class="popContent popContentHost" id='hosts'>	
				<div class="newHost" id="addHostLine">
					<input type="text" id="newHostName" name="hosts" placeholder="...nom..." onsubmit="return(false);">
					<img src="public/images/add.png" alt="Bouton Ajouter Hôte" id="addHostBtn" class="hostToBeAdded hostIcon">
				</div>
				<div id="ourHosts">						
					<?php if(!empty($hosts)) { 
						foreach ($hosts as $host) { ?>
						<div id="<?= 'host'.$host -> id() ?>" class="hostList">
							<?= $host -> name() ?>
							<img src="public/images/koS.png" id='<?= $host -> id() ?>' alt="Bouton Retirer Hôte" class="hostIcon hostToBeDeleted">
						</div>
						<?php }
					} ?>
				</div>
			</div>
		</div>

		<div class="pop">
			<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-down"></span>Mon commentaire</h2>
			<div class="popContent ">
				<div class="comment">
					<textarea name="comment" rows="10" placeholder='...un commentaire...'/><?= $comment ?></textarea>
				</div>		
			</div>
		</div>	
		<div class="pop">
			<h2 class="popTitleRequest presentation">Texte de présentation pour le site web PEL officiel</h2>
			<div id="webContent">
				<div class="container">
					<div id="contentTextareaBtn">Écrire une présentation 
						<span id="down" class="fas fa-caret-down"></span>			
					</div><!--
				 --><div id="contentFileBtn">Ou déposer un fichier
						<span id="downBis" class="fas fa-caret-down"></span> 
					</div>
				</div>
		        <div id="contentTextarea" class="invisible">
					<textarea name="content" rows="5" placeholder='...votre présentation...'/><?= $content ?></textarea>	
				</div>
		        <div id="contentFile" class="invisible">
					<?= $contentDoc ?>	
					<input type="file" id="upload" class="upload" name="contentDoc"/>
					<label for="upload">
						<img width="64" height="64" class="uploadIcon" title="Cliquez pour déposer un fichier présentation" alt="Déposer un fichier" src="public/images/upload.png" />
						<div class="uploadFilename"><em class="uploadFilename">Fichiers .doc/.docx/.odt uniquement</em></div>
					</label>
				</div>			
			</div>
		</div>			
		<input type="submit" name="myBooth" value="Valider" id="submitBtn"/>

	</form>
</div>
<?php $content = ob_get_clean();  

ob_start(); ?>
	    <script src="public/js/YesNo.js"></script>
	    <script src="public/js/Host.js"></script>
	    <script src="public/js/Game.js"></script>
	    <script src="public/js/Counter.js"></script>
	    <script src="public/js/FixedSubmitBtn.js"></script>
	    <script src="public/js/booth.js"></script>
<?php $java = ob_get_clean(); 

require('templateUser.php'); ?>