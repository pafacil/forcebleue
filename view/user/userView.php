<?php
$title = 'ForceBleue - Welcome'; 
$head_options = ''; 

ob_start(); ?>
<div id="default">
	<h1>Bienvenue sur votre page <br><span class="lightBlue">Force Bleue</span></h1>
	<div class="pop">
		<h2 class="popTitle"><span class="fas fa-caret-down"></span>Présentation</h2>
		<div class="popContent">	
			<p>Vous voilà sur le site <strong>Force Bleue</strong> du Festival <strong>"Paris Est Ludique"</strong>. Sur ce site, vous allez pouvoir renseigner les informations qui nous permettront de vous accueillir au mieux sur le Festival.</p>
			<p>Dans un premier temps, il vous faudra nous donner des informations sur l'onglet "Profil". Puis, sur l'onglet "Stand", vous pourrez nous décrire le stand désiré. Enfin, l'onglet "Récap" vous propose un résumé de votre commande.</p>
			<p>Vous trouverez également un onglet FAQ pour la Foire aux Questions ainsi qu'un onglet Contacts pour nous joindre.</p>			
		</div>
	</div>
	<div class="pop">
		<h2 class="popTitle"><span class="fas fa-caret-down"></span>Politique de cookie, RGPD</h2>
		<div class="popContent">			
			<p>Sachez que nous utilisons simplement une information de session temporaire pour faciliter votre navigation (nous ne posons pas de cookies à long terme sur votre ordinateur). Vous avez par ailleurs un droit de regard sur les informations que vous nous avez confiées pour la bonne marche de votre stand sur le Festival. Vous pouvez ainsi à tout moment nous demander de les consulter ou les effacer (en nous contactant par le formulaire par exemple). Elles se limitent à ce que vous trouverez dans les pages "Profil" et "Stand".</p>
			<p>Nous utilisons ces données uniquement pour notre Festival et elles ne sont transmises à aucun Tiers.</p>
			<p>Si vous ne souhaitez pas laisser vos informations sur ce site, vous pouvez nous joindre directement par mail à <strong><a href="mailto:?to=contact@parisestludique.fr&subject=Inscription%20Exposant%20Hors%20Informatique">contact@parisestludique.fr</a></strong> et nous traiterons votre commande par mail ou téléphone, sans sauvegarde d'information sur le net.</p>	
		</div>
	</div>
	<div class="pop">
		<h2 class="popTitle"><span class="fas fa-caret-down"></span>Menu principal</h2>
		<div class="popContent">
			<ul>
				<li><strong><a href="index.php">Accueil</a></strong> : vous y êtes !</li>
				<li><strong><a href="index.php?action=myProfile">Profil</a></strong> : pour voir / modifier les infos</li>
				<li><strong><a href="index.php?action=myBooth">Stand</a></strong> : pour définir mon stand</li>
				<li><strong><a href="index.php?action=getStatus">Récap</a></strong> : un récapitulatif de ma situation</li>
				<li><strong><a href="index.php?action=faq">FAQ</a></strong> : les questions que vous vous posez</li>
				<li><strong><a href="index.php?action=contactUs">Contacts</a></strong> : pour nous joindre</li>
			</ul>
		</div>
	</div>	
</div>

<?php $content = ob_get_clean(); 

ob_start(); 
$java = ''; 

require('templateUser.php'); ?>