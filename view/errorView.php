<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
	    <title>Paris Est Ludique - ForceBleue - Erreur</title>
	    <link rel="icon" type="image/x-icon" href="public/images/favicon.png" />
	    <link href="public/css/style.css" rel="stylesheet" /> 
	</head>
	<body>
		<div class="overlay">
			<div id='error'>
	            <div>
	            	<h1></h1>
	            	<h2>Erreur rapportée</h2>
        		    <p><?= $errorMsg ?></p>
			        <p class="backHome"><a href="index.php">Retour au site</a></p>
				</div>
			</div>			
		</div>
	</body>
</html>
