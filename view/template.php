<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
	    <title><?= $title ?></title>
	    <link href="public/css/style.css" rel="stylesheet" /> 
	    <link rel="icon" type="image/x-icon" href="public/images/favicon.png" />
	    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet"> 
	    <?= $head_options ?>
	</head>
	<body>

		<header>
			<div id="headerContent">
				<h1>Paris Est Ludique</h1>
				<h2>Force Bleue</h2>
			</div>
			<?= $menu ?>
		</header>

		<span class="scrollBtn"></span>
	    <?php if (isset($tempMsg)) { ?>
			<div id="tempMessage">
				<h1>
					<?= $tempMsg ?>        	
				</h1>
			</div>
		<?php } ?>

	    <?= $content ?>

		<footer>
			<div id="footerContent">
	            © Copyright - Tous droits réservés - 2018          
	        </div>
		</footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<?= $java ?>
	</body>
</html>