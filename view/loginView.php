<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
	   	<meta name="description" content="ForceBleue est le site dédié aux exposants de Paris Est Ludique. Il nous permet de référencer le statut d'avancement des contrats avec chaque éditeur, boutique ou association." />  
	    <title>Paris Est Ludique - ForceBleue - Login</title>
	    <link rel="icon" type="image/x-icon" href="public/images/favicon.png" />
	    <link href="public/css/style.css" rel="stylesheet" /> 
	    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet"> 
	</head>
	<body>

		<span class="scrollBtn"></span>
	    <?php if (isset($tempMsg)) { ?>
			<div id="tempMessage">
				<?= $tempMsg ?>        	
			</div>
		<?php } ?>

		<div class="overlay">
			<div id='login' <?php if (isset($newPassword)) {echo 'class="newPassword"';} ?>>
	            <div>
	            	<div id='loginHeader'></div>
			        <form action="<?= $action ?>" method="post">
			            <?= $passwordFormMsg ?>
			            <?= $warningMessage ?>						  
					</form>  
					<?= $pwdMsg ?>
				</div>
				<div id="passwordRules">
				  <h3>Le mot de passe doit contenir :</h3>
				  <p id="lowerChk" class="invalid"><b>1 minuscule</b> au moins</p>
				  <p id="upperChk" class="invalid"><b>1 majuscule</b> au moins</p>
				  <p id="numberChk" class="invalid"><b>1 chiffre</b> au moins</p>
				  <p id="lengthChk" class="invalid"><b>8 caractères</b> au moins</p>
				  <p id="specialChk" class="invalid"><b>1 caractère spécial</b> ( #$%&!@?(){}[] ) au moins</p>
				  <h3>OU être composé de :</h3>
				  <p id="ultraLengthChk" class="invalid"><b>20 caractères</b> au moins</p>
				  <h3>Et forcément :</h3>
				  <p id="identicalChk" class="invalid"><strong>Les deux entrées doivent être identiques</strong></p>
				</div>
			</div>			
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	    <?= $java ?>
	    <script src="public/js/main.js"></script>
	</body>
</html>
