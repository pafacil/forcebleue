<?php
$title = 'ForceBleue - Administration'; 
$head_options = ''; 

ob_start(); ?>
<h1 id='exhibPresentation'>	
	<?php if ($exhibitor -> logo() != '') { ?>
		<img src="public/userData/logo/<?= $exhibitor -> logo() ?>" alt="<?= $exhibitor -> name() ?>"  class="logoIcon"> 
	<?php } else { ?>
	<div id="noLogo">
		<img class="noLogoImg" src="public/images/noLogoOff.png" alt="Pas de Logo">
		<img class="noLogoImg" src="public/images/noLogoOn.png" alt="Pas de Logo">
	</div> <?php } ?>
	<?= $exhibitor -> name() ?>
	<?php $arrayType = ['' => 'Type Indéfini', 'boutique' => 'Boutique', 'association' => 'Association', 'editeur' => 'Éditeur'];
		$arrayTypeSrc = ['' => 'undefined.png', 'boutique' => 'typeShopOn.png', 'association' => 'typeAssociationOn.png', 'editeur' => 'typePublisherOn.png']; 
		$imgTypeSrc = $arrayTypeSrc[$exhibitor -> type()];
		$imgType = $arrayType[$exhibitor -> type()]; ?>
		<img src="public/images/<?= $imgTypeSrc ?>" alt="Type" class="typeIcon" title="<?= $imgType ?>">
</h1>	
<div id="listExhibitor">
	<h2 class="link" onclick="document.location='index.php?action=getStatus'">
		<div class="list">Retour à la liste</div>
	</h2>

	<div class="pop">
		<h2 class="popTitle popTitleInfos"><span class="fas fa-caret-down"></span>Contact Principal</h2>
		<div class="popContent container">
			<table>
				<tr>
					<th>Prénom</th>
					<td><?= $exhibitor -> firstName1() ?></td>
				</tr>
				<tr>
					<th>Nom</th>
					<td><?= $exhibitor -> lastName1() ?></td>
				</tr>
				<tr>
					<th>Mail</th>
					<td><?= $exhibitor -> mail1() ?></td>
				</tr>
				<tr>
					<th>Téléphone</th>
					<td><?= $exhibitor -> phone1() ?></td>
				</tr>											
			</table>
		</div>
	</div>

	<?php if (!empty($exhibitor -> mail2()) || !empty($exhibitor -> phone2)) { ?>
	<div class="pop">
		<h2 class="popTitle popTitleInfos"><span class="fas fa-caret-down"></span>Contact Secondaire</h2>
		<div class="popContent container">
			<table>
				<tr>
					<th>Prénom</th>
					<td><?= $exhibitor -> firstName2() ?></td>
				</tr>
				<tr>
					<th>Nom</th>
					<td><?= $exhibitor -> lastName2() ?></td>
				</tr>
				<tr>
					<th>Mail</th>
					<td><?= $exhibitor -> mail2() ?></td>
				</tr>
				<tr>
					<th>Téléphone</th>
					<td><?= $exhibitor -> phone2() ?></td>
				</tr>											
			</table>
		</div>
	</div>
	<?php } ?>


	<div class="pop">
		<h2 class="popTitle popTitleInfos"><span class="fas fa-caret-down"></span>Facturation</h2>
		<div class="popContent container">
			<table>
				<tr>
					<th>Compagnie</th>
					<td><?= $exhibitor -> company() ?></td>
				</tr>
				<tr>
					<th>Rue</th>
					<td><?= $exhibitor -> street() ?></td>
				</tr>
				<tr>
					<th>Code postal</th>
					<td><?= $exhibitor -> postalCode() ?></td>
				</tr>
				<tr>
					<th>Ville</th>
					<td><?= $exhibitor -> city() ?></td>
				</tr>
				<tr>
					<th>Pays</th>
					<td><?= $exhibitor -> country() ?></td>
				</tr>	
				<tr>
					<th>Signataire</th>
					<td><?= $exhibitor -> contractor() ?></td>
				</tr>
				<tr>
					<th>VAT</th>
					<td><?= $exhibitor -> vatNumber() ?></td>
				</tr>											
			</table>
		</div>
	</div>

	<div class="pop">
		<h2 class="popTitle popTitleInfos"><span class="fas fa-caret-down"></span>Communication</h2>
		<div class="popContent container">
			<table>
				<tr>
					<th class="titleIcon webUrl"></th>
					<td><?= $exhibitor -> webUrl() ?></td>
				</tr>
				<tr>
					<th class="titleIcon facebook"></th>
					<td><?= $exhibitor -> facebook() ?></td>
				</tr>
				<tr>
					<th class="titleIcon twitter"></th>
					<td><?= $exhibitor -> twitter() ?></td>
				</tr>											
			</table>		
		</div>
	</div>	

	<div class="pop">
		<h2 class="popTitle popTitleInfos"><span class="fas fa-caret-down"></span>Contenu sur la page PEL</h2>
		<div class="popContent container">
			<table>
				<tr>
					<th colspan="2">Contenu texte</th>
				</tr>
				<tr>
					<td colspan="2"><?php echo ($exhibitor -> content() == '') ? '-': $exhibitor -> content(); ?></td>
				</tr>
				<tr>
					<th style="border: none; text-align: right; padding-right: 20px">Fichier</th>
					<td><?= $contentDoc ?></td>
				</tr>										
			</table>
		</div>
	</div>

	<?php if ($exhibitor -> comment() != '') { ?>
	<div class="pop">
		<h2 class="popTitle popTitleInfos"><span class="fas fa-caret-down"></span>Commentaire</h2>
		<div class="popContent container">
			<p class="popComment"><?= $exhibitor -> comment() ?></p>
		</div>
	</div>
	<?php } ?>

	<div class="pop">
		<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-down"></span>Pack Requis <?php echo ($arrayType[$exhibitor -> type()] == 'Type Indéfini') ? '': $arrayType[$exhibitor -> type()]; ?></h2>
		<div class="popContent container">
			<table>
				<?php if ($exhibitor -> type() == 'boutique') { ?>
				<tr>
					<th>Boutique</th>
					<th>Voir avec la boutique pour fixer les modalités de vente.</th>
				</tr>				
				<?php } else if ($exhibitor -> type() == 'association') { ?>
				<tr>
					<th>Association</th>
					<td><?= $pack -> association() ?></td>
				</tr>				
				<tr>
					<th>Extérieur</th>
					<td><?= $pack -> associationExterieur() ?></td>
				</tr>		
				<?php } else if ($exhibitor -> type() == 'editeur') { ?>
				<tr>
					<th>Special</th>
					<td><?= $pack -> special() ?></td>
				</tr>	
				<tr>
					<th>Double</th>
					<td><?= $pack -> doublePack() ?></td>
				</tr>
				<tr>
					<th>Standard</th>
					<td><?= $pack -> standard() ?></td>					
				</tr>							
				<tr>
					<th>Mini</th>
					<td><?= $pack -> mini() ?></td>					
				</tr>
				<tr>
					<th>Table</th>
					<td><?= $pack -> tablePack() ?></td>					
				</tr>	
				<?php } else { ?>
				<tr>
					<th>Type indéfini (éditeur, boutique, association)</th>
				</tr>							
				<?php }?>
			</table>
		</div>
	</div>

	<?php if ($exhibitor -> pass() != '0' && $exhibitor -> invitation() != '0') { ?>
	<div class="pop">
		<h2 class="popTitle popTitleRequest"><span class="fas fa-caret-down"></span>Billets</h2>
		<div class="popContent">		
			<table>
				<tr>
					<th>Pass</th>
					<td><?= ($exhibitor -> pass() == '') ? '0': $exhibitor -> pass(); ?></td>
				</tr>				
				<tr>
					<th>Invitation</th>
					<td><?= ($exhibitor -> invitation() == '') ? '0': $exhibitor -> invitation(); ?></td>
				</tr>
			</table>
		</div>
	</div>		
	<?php } ?>		

	<?php if (!empty($games)) { ?>
	<div class="pop">
		<h2 class="popTitle popTitleExposed"><span class="fas fa-caret-down"></span>Jeux présentés</h2>
		<div class="popContent container">		
			<table>		
				<?php 
				$arrayGameType = [
					'new' => 'Nouveauté',
					'classic' => 'Classique',
					'proto' => 'Proto'
				];
				foreach($games AS $game) { ?>
					<tr>
						<th><?= $game -> title() ?></td>					
						<td><?= $arrayGameType[$game -> classification()] ?></td>
					</tr>
				<?php }	?>
			</table>
		</div>
	</div>		
	<?php } ?>

	<?php if (!empty($hosts)) { ?>
	<div class="pop">
		<h2 class="popTitle popTitleExposed"><span class="fas fa-caret-down"></span>Partenaires hébergés</h2>
		<div class="popContent container">		
			<table>		
				<?php 
				foreach($hosts AS $host) { ?>
					<tr>
						<td><?= $host -> name() ?></td>
					</tr>
				<?php }	?>
			</table>
		</div>
	</div>		
	<?php } ?>		
</div>
<?php $content = ob_get_clean(); 

ob_start(); 
$java = ''; 

require('templateAdmin.php'); ?>