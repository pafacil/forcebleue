<?php
$title = 'ForceBleue - Administration'; 
$head_options = ''; 

ob_start(); ?>
<h1>Exposants - Consultation</h1>	
<div id="listExhibitors">
	<h2 class="link" onclick="document.location='index.php?action=modifyStatus'">
		<div class="update">Modification</div>
	</h2>	
	<div id="selectExhib">
		<img src="public/images/selectionOff.png" alt="Icône sélection" class="selectionIcon"  id="selectionIcon">
		<?php 
		selection('exhibitor','1','multiple','Aucun exposant sélectionné','','calc(100% - 34px)','exhibitorToSelect',''); ?>
	</div>
	<table>
		<colgroup>
	        <col class="exhibName">
	        <col span="5" class="exhibStatus">
	        <col class="exhibCheck">
	    </colgroup>
		<tr class='titleLine'>
			<th title="Nom de l'exposant" class="titleIcon exhibitor"></th>
			<th title="Mail de confirmation" class="titleIcon mail"></th>
			<th title="Première connexion au site" class="titleIcon connection"></th>
			<th title="Alpha" class="titleIcon alpha"></th>
			<th title="Contrat signé" class="titleIcon contract"></th>
			<th title="Numéro de stand" class="titleIcon booth"></th>
			<th title="Validation du profil" class="titleIcon check"></th>	
		</tr>
		<?php 
		foreach($listExhibitors AS $exhibitor) { ?>
			<tr class="exhibLine">
				<td><strong><a href="index.php?action=getExhibitor&exhibitor=<?= $exhibitor -> id() ?>"><?= $exhibitor -> name() ?></a></strong></td>
				<td <?php if ($exhibitor -> confirmation() == '1') {
					echo "class='okS'";
				} else {
					echo "class='koS'";
					$pendingProfile = 1;
				} ?>>
				</td>
				<td <?php if ($userManager -> exists($exhibitor -> userId())) {
					$user = $userManager -> getUser($exhibitor -> userId());
					if ($user -> privilege() == 'newUser') {
						echo "class='koS'";
						$pendingProfile = 1;
					} else {
						echo "class='okS'";
					}
				} ?>>
				</td>
				<td <?php if ($exhibitor -> alpha() == '1') {
					echo "class='okS'";
				} else {
					echo "class='koS'";
					$pendingProfile = 1;
				} ?>>
				</td>
				<td <?php if ($exhibitor -> signed() == '1') {
					echo "class='okS'";
				} else {
					echo "class='koS'";
					$pendingProfile = 1;
				} ?>>
				</td>			
				<td class="stand">
					<?php if ($exhibitor -> stand() == '') {
						echo "-";
						$pendingProfile = 1;
					} else {
						echo $exhibitor -> stand();
					} ?>					
				</td>				
				<td <?php if (isset($pendingProfile)) {
					echo "class='koM'";
					unset($pendingProfile);
				} else {
					echo "class='okM'";
				} ?>>
				</td>
			</tr>
		<?php } ?>
	</table>
</div>
<?php $content = ob_get_clean();  

ob_start(); ?>
	    <script src="public/js/Select.js"></script>
<?php $java = ob_get_clean(); 

require('templateAdmin.php'); ?>