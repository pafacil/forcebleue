<?php
$title = 'ForceBleue - Administration'; 
$head_options = ''; 

ob_start(); ?>	
<h1>Exposants - Update</h1>	
<form action="index.php?action=updateStatus" id="exhibitorsStatusForm" method="post">
	<div id="listExhibitors">
		<h2 class="link" onclick="document.location='index.php?action=getStatus'">
			<div class="reading">Consultation</div>
		</h2>		
		<div id="selectExhib">
			<img src="public/images/selectionOff.png" alt="Icône sélection" class="selectionIcon" id="selectionIcon">
			<?php 
			selection('exhibitor','1','multiple','Aucun exposant sélectionné','','calc(100% - 34px)','exhibitorToSelect',''); ?>
		</div>		
		<table>
			<colgroup>
		        <col class="exhibName">
		        <col span="4" class="exhibModifyStatus">
		    </colgroup>
			<tr class= "titleLine">
				<th title="Nom de l'exposant" class="titleIcon exhibitor"></th>
				<th title="Mail de confirmation" class="titleIcon mail"></th>
				<th title="Alpha" class="titleIcon alpha"></th>
				<th title="Contrat signé" class="titleIcon contract"></th>
				<th title="Numéro de stand" class="titleIcon booth"></th>
			</tr>
			<?php 
			foreach($listExhibitors AS $exhibitor) { 
				$confirmation = $exhibitor -> id().'[confirmation]';
				$alpha = $exhibitor -> id().'[alpha]';
				$signed = $exhibitor -> id().'[signed]';
				$stand = $exhibitor -> id().'[stand]';
				?>
				<tr class="exhibLine" id="<?= $exhibitor -> id() ?>">	
					<td><strong><a href="index.php?action=getExhibitor&exhibitor=<?= $exhibitor -> id() ?>"><?= $exhibitor -> name() ?></a></strong></td>
					<td>
						<div class="flip-container <?php if ($exhibitor -> confirmation() == '1') {echo 'flip';} ?>" >
							<div class="flipper">
								<div class="koB">
								</div>
								<div class="okB">
								</div>
							</div>
						</div>
						<input type="checkbox" name="<?= $confirmation?>" id="<?= $confirmation?>" class="hidden" <?php if ($exhibitor -> confirmation() == '1') {echo 'checked';} ?>>
					</td>
					<td>
						<div class="flip-container <?php if ($exhibitor -> alpha() == '1') {echo 'flip';} ?>" >
							<div class="flipper">
								<div class="koB">
								</div>
								<div class="okB">
								</div>
							</div>
						</div>
						<input type="checkbox" name="<?= $alpha?>" id="<?= $alpha?>" class="hidden" <?php if ($exhibitor -> alpha() == '1') {echo 'checked';} ?>>						
					</td>
					<td>
						<div class="flip-container <?php if ($exhibitor -> signed() == '1') {echo 'flip';} ?>" >
							<div class="flipper">
								<div class="koB">
								</div>
								<div class="okB">
								</div>
							</div>
						</div>
						<input type="checkbox" name="<?= $signed?>" id="<?= $signed?>" class="hidden" <?php if ($exhibitor -> signed() == '1') {echo 'checked';} ?>>						
					</td>						
					<td><input type="number" name="<?= $stand ?>" placeholder="<?php echo (($exhibitor -> stand() == '') ? '-' : $exhibitor -> stand()); ?>"/></td>				
				</tr>
			<?php } ?>
		</table>
	</div>
	<input type="submit" name="exhibitorsStatus" value="Valider" id="submitBtn"/>					
</form>
<?php $content = ob_get_clean(); 

ob_start(); ?>
	    <script src="public/js/FixedSubmitBtn.js"></script>
	    <script src="public/js/Select.js"></script>
	    <script src="public/js/YesNo.js"></script>
<?php $java = ob_get_clean(); 

require('templateAdmin.php'); ?>