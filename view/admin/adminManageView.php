<?php
$title = 'ForceBleue - Administration'; 
$head_options = ''; 

ob_start(); ?>
<h1>Gestion</h1>	
<div id="controls">
	<div class="action">
		<div id="addExhibitorBtn" class="actionBtn"><div class="actionTitle"><span class="darkBlue">Ajouter</span> <span class="orange">un</span> exposant</div>
			<img src="public/images/addExhibitor.png" alt="Image Ajouter un Exposant">	
			<div id="addExhibitor" class="closed actionForm">
				<form action="index.php?action=addExhibitor" method="post">
					<div class="actionFormTitle"><h2>Ajouter un nouvel exposant</h2>
					<p>(tous les champs obligatoires)</p></div>			
					<input type="mail" name="exhibitorName" placeholder='Nom exposant...' id="addExhibitorFirstFocus" required/><br>	
					<input type="mail" name="userMail" placeholder='Mail exposant...' required/><br>
					<input type="text" name="userFirstName" placeholder='Prénom...' required/><br>
					<input type="text" name="userLastName" placeholder='Nom...' required/><br>		
					<input type="submit" name="mailCreation" class="add" value="Créer + Mail"/>			
				</form>
			</div>
		</div>

		<div id="removeExhibitorBtn" class="actionBtn"><div class="actionTitle"><span class="darkBlue">Supprimer</span> <span class="orange">un</span> exposant</div>
			<img src="public/images/removeExhibitor.png" alt="Image Retirer un Exposant">			
			<div id="removeExhibitor" class="closed actionForm">
				<form action="index.php?action=removeExhibitor" method="post">
					<div class="actionFormTitle"><h2>Retirer un exposant</h2><br></div>		
					<?php 
					selection('exhibitorAndUser','1','single','Aucun exposant sélectionné','','90%','exhibitorToDelete',''); ?>
					<input type="submit" name="exhibitorDeletion" class="remove" value="Supprimer"/>
				</form>
			</div>
		</div>

	</div>
	<div class="action">
		<div id="addUserBtn" class="actionBtn"><div class="actionTitle"><span class="darkBlue">Ajouter</span> <span class="orange">un</span> admin</div>
			<img src="public/images/addUser.png" alt="Image Ajouter un Utilisateur">
			<div id="addUser" class="closed actionForm">
				<form action="index.php?action=addAdmin" method="post">
					<div class="actionFormTitle"><h2>Ajouter un admin</h2>
					<p>(tous les champs obligatoires)</p></div>	
					<input type="text" name="userFirstName" placeholder='Prénom admin...' id="addUserFirstFocus" required/><br>
					<input type="text" name="userLastName" placeholder='Nom admin...' required/><br>
					<input type="mail" name="userMail" placeholder='Mail admin...' required/><br>
					<input type="submit" name="mailCreation" class="add" value="Créer + Mail"/>	
				</form>
			</div>
		</div>
	
		<div id="removeUserBtn" class="actionBtn"><div class="actionTitle"><span class="darkBlue">Supprimer</span> <span class="orange">un</span> utilisateur</div>
			<img src="public/images/removeUser.png" alt="Image Retirer un Utilisateur">
			<div id="removeUser" class="closed actionForm">
				<form action="index.php?action=removeUser" method="post">
					<div class="actionFormTitle"><h2>Retirer un utilisateur</h2><br></div>
					<?php 
					selection('userAndExhib','1','single','Aucun utilisateur sélectionné','','90%','userToDelete',''); ?>
					<input type="submit" name="userDeletion" class="remove" value="Supprimer"/>		
				</form>
			</div>
		</div>
	
	</div>
</div>

<?php $content = ob_get_clean(); 

ob_start(); 
$java = ''; 
require('templateAdmin.php'); ?>