<?php
$title = 'ForceBleue - Administration'; 
$head_options = ''; 

ob_start(); ?>
<div id="default">
	<h1>Welcome, cher admin !</h1>
	<p>Ce site nous permet de gérer les différents exposants.<br>Ici, côté backend, nous avons la main sur la création de compte Exposant, Admin.<br>Nous pouvons aussi vérifier l'avancement de la situation de chaque Exposant.</p>
	<div class="pop">
		<h2 class="popTitle"><span class="fas fa-caret-down"></span>Gestion</h2>
		<div class="popContent">	
			<ul>
				<li>Ajouter un exposant</li>
				<li>Ajouter un administrateur</li>
				<li>Supprimer un exposant (et l'utilisateur lié)</li>
				<li>Supprimer un utilisateur/administrateur</li>
			</ul>
		</div>
	</div>
	<div class="pop">
		<h2 class="popTitle"><span class="fas fa-caret-down"></span>Exposants</h2>
		<div class="popContent">	
			<ul>
				<li>Suivre l'état d'avancement des situations des exposants</li>
				<li>Modifier l'état d'avancement des exposants</li>
				<li>Consulter les informations d'un exposant</li>
			</ul>
		</div>
	</div>	
</div>
<?php $content = ob_get_clean(); 

$java = ''; 

require('templateAdmin.php'); ?>